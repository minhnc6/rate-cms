package vn.yotel.ratecms.model;

import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.jpa.RpService;

import java.util.Date;

public class ServicePriceModel {
    private Integer priceId;
    private RpOrganization orgId;
    private RpService serviceId;
    private String serviceName;
    private Date startDatetime;
    private Date endDatetime;
    private Double price;
    private String regex;
    private String priceStatus;

    public Integer getPriceId() {
        return priceId;
    }

    public void setPriceId(Integer priceId) {
        this.priceId = priceId;
    }

    public RpOrganization getOrgId() {
        return orgId;
    }

    public void setOrgId(RpOrganization orgId) {
        this.orgId = orgId;
    }

    public RpService getServiceId() {
        return serviceId;
    }

    public void setServiceId(RpService serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Date getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getPriceStatus() {
        return priceStatus;
    }

    public void setPriceStatus(String priceStatus) {
        this.priceStatus = priceStatus;
    }
}
