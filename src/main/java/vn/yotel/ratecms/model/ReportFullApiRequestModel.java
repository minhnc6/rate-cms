package vn.yotel.ratecms.model;

public class ReportFullApiRequestModel {
    private String tctd;
    private double freePoint;
    private double paidPoint;
    private double allPoint;
    private double freeCus;
    private double paidCus;
    private double allCus;
    private double freeFrau;
    private double paidFrau;
    private double allFrau;

    public String getTctd() {
        return tctd;
    }

    public void setTctd(String tctd) {
        this.tctd = tctd;
    }

    public double getFreePoint() {
        return freePoint;
    }

    public void setFreePoint(double freePoint) {
        this.freePoint = freePoint;
    }

    public double getPaidPoint() {
        return paidPoint;
    }

    public void setPaidPoint(double paidPoint) {
        this.paidPoint = paidPoint;
    }

    public double getAllPoint() {
        return allPoint;
    }

    public void setAllPoint(double allPoint) {
        this.allPoint = allPoint;
    }

    public double getFreeCus() {
        return freeCus;
    }

    public void setFreeCus(double freeCus) {
        this.freeCus = freeCus;
    }

    public double getPaidCus() {
        return paidCus;
    }

    public void setPaidCus(double paidCus) {
        this.paidCus = paidCus;
    }

    public double getAllCus() {
        return allCus;
    }

    public void setAllCus(double allCus) {
        this.allCus = allCus;
    }

    public double getFreeFrau() {
        return freeFrau;
    }

    public void setFreeFrau(double freeFrau) {
        this.freeFrau = freeFrau;
    }

    public double getPaidFrau() {
        return paidFrau;
    }

    public void setPaidFrau(double paidFrau) {
        this.paidFrau = paidFrau;
    }

    public double getAllFrau() {
        return allFrau;
    }

    public void setAllFrau(double allFrau) {
        this.allFrau = allFrau;
    }
}
