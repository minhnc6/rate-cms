package vn.yotel.ratecms.model;

import java.util.Date;

public class CampaignFullModel {
    private Date receiveDatetime;
    private String orgName;
    private double total;

    public Date getReceiveDatetime() {
        return receiveDatetime;
    }

    public void setReceiveDatetime(Date receiveDatetime) {
        this.receiveDatetime = receiveDatetime;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
