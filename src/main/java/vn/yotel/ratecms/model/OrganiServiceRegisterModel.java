package vn.yotel.ratecms.model;

import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.jpa.RpService;

import java.util.Date;

public class OrganiServiceRegisterModel {
    private Integer id;
    private RpOrganization orgId;
    private RpService serviceId;
    private String ipList;
    private String token;
    private String registerStatus;
    private String serviceName;
    private Date expireDatetime;

    public Date getExpireDatetime() {
        return expireDatetime;
    }

    public void setExpireDatetime(Date expireDatetime) {
        this.expireDatetime = expireDatetime;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getIpList() {
        return ipList;
    }

    public void setIpList(String ipList) {
        this.ipList = ipList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RpOrganization getOrgId() {
        return orgId;
    }

    public void setOrgId(RpOrganization orgId) {
        this.orgId = orgId;
    }

    public RpService getServiceId() {
        return serviceId;
    }

    public void setServiceId(RpService serviceId) {
        this.serviceId = serviceId;
    }

    public String getRegisterStatus() {
        return registerStatus;
    }

    public void setRegisterStatus(String registerStatus) {
        this.registerStatus = registerStatus;
    }
}
