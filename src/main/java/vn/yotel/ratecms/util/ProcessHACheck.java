package vn.yotel.ratecms.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class ProcessHACheck {
	private static final Logger logger = LoggerFactory.getLogger(ProcessHACheck.class);

	private static String vipAddr_1 = "10.58.99.110";
	private static String vipAddr_2 = "10.58.99.111";
	private static int port = 8080;

	public static boolean isActiveServer() {
		boolean result = false;
		try {
			List<String> ips = getIpAddress();
			boolean reachable = isConnect(vipAddr_1,port,1000);
			if (reachable && ips.contains(vipAddr_1)) {
				result = true;
			}else if(!reachable && ips.contains(vipAddr_2)){
				result = true;
			}
		} catch (Exception e) {
			logger.error("Error while getting address information: {}", e.getMessage());
		}
		return result;
	}

	private static boolean isConnect(String ip, int port, int timeout) {
		try {
			Socket socket = new Socket();
			socket.connect(new InetSocketAddress(ip, port), timeout);
			socket.close();
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public static List<String> getIpAddress() throws SocketException {
		List<String> result = new ArrayList<String>();
		Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
		for (NetworkInterface netint : Collections.list(nets)) {
			Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
			for (InetAddress inetAddress : Collections.list(inetAddresses)) {
				result.add(inetAddress.getHostAddress());
			}
		}
		return result;
	}
}
