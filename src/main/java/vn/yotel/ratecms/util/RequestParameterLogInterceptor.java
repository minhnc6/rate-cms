package vn.yotel.ratecms.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RequestParameterLogInterceptor extends HandlerInterceptorAdapter{

    private static final Logger logger = LoggerFactory.getLogger(RequestParameterLogInterceptor.class);

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        if(logger.isDebugEnabled()){
            // parameters are contained in the query string or posted form data.
            logger.debug("Request Log !");
            logger.debug("Request URI[{}]" ,request.getRequestURI());
            logger.debug("Request Method[{}]" ,request.getMethod());

            // headers
            Map<String,String>  headerMap = new HashMap<String, String>();
            Enumeration<String> headerNames = request.getHeaderNames();
            while(headerNames.hasMoreElements()){
                String headerName = headerNames.nextElement();
                String headerValue = request.getHeader(headerName);
                headerMap.put(headerName, headerValue);
            }
            logger.debug("Request Headers[{}]" ,headerMap);
            logger.debug("Request Parameters[{}]",objectMapper.writeValueAsString(request.getParameterMap()));
        }

        return super.preHandle(request, response, handler);
    }

}
