package vn.yotel.ratecms.web.controller;

import com.google.common.base.Strings;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.apache.commons.collections4.CollectionUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import vn.yotel.ratecms.Constans;
import vn.yotel.ratecms.jpa.*;
import vn.yotel.ratecms.model.OrganiServiceRegisterModel;
import vn.yotel.ratecms.model.ServicePriceModel;
import vn.yotel.ratecms.service.*;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/organization")
public class RpOrganizationController {

    private static final Logger LOG = LoggerFactory.getLogger(RpOrganizationController.class);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private SimpleDateFormat sdfApi = new SimpleDateFormat("yyyy-MM-dd");

    @Value("${api.token}")
    private String apiToken;

    @Autowired
    RpOrganizationService rpOrganizationService;

    @Autowired
    RpProductService rpProductService;

    @Autowired
    RpServicePriceService rpServicePriceService;

    @Autowired
    RpServiceService rpServiceService;

    @Autowired
    RpOrgServiceRegisterService rpOrgServiceRegisterService;

    @Autowired
    RpActionAuditService rpActionAuditService;

    @RequestMapping(value = "/list.html", method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String list(Model model,
                              @RequestParam(value = "strName", defaultValue = "") String name,
                              @RequestParam(value="strCode" , defaultValue = "") String code,
                              @RequestParam(value="strStt" , defaultValue = "") String status,
                              Pageable pageable) {
        String not_found_message = "";

        Pageable _pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize());
        Page<RpOrganization> pageData;
        if (Strings.isNullOrEmpty(name)) {
            name = null;
        }
        if(Strings.isNullOrEmpty(code)) {
            code = null;
        }
        if(Strings.isNullOrEmpty(status)) {
            status = null;
            pageData = rpOrganizationService.findPage(name, code, null, _pageable);
        } else {
            pageData = rpOrganizationService.findPage(name, code, status, _pageable);
        }

        if (pageData.getContent().size() == 0) {
            not_found_message = Constans.NOT_FOUND_MESSAGE;
        }
        model.addAttribute("page", pageData);
        model.addAttribute("name", name);
        model.addAttribute("code", code);
        model.addAttribute("status", status);
        model.addAttribute("not_found_message", not_found_message);
        return "organization/list";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addOrganizationPost(Model model,
                                  Principal principal,
                                  @RequestParam(value="strName" , defaultValue = "") String name,
                                  @RequestParam(value="strCode" , defaultValue = "") String code,
                                  @RequestParam(value="strAdd" , defaultValue = "") String address,
                                  @RequestParam(value="strPhone" , defaultValue = "") String phone,
                                  @RequestParam(value="strTax" , defaultValue = "") String taxNo,
                                  @RequestParam(value="strDes" , defaultValue = "") String description,
                                  @RequestParam(value="strStt" , defaultValue = "") String status,
                                  @RequestParam(value="ipList" , defaultValue = "") String ipList,
                                  @RequestParam(value="expiredDate" , defaultValue = "") String expiredDate,
                                  @RequestParam(value="strService" , defaultValue = "") String strService,
                                  @RequestParam(value="orgType" , defaultValue = "1") String orgType) {
        String message = "Error";
        try {
            Date expired = sdf.parse(expiredDate);
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(code)
                    && !Strings.isNullOrEmpty(status)) {
                RpOrganization rp = new RpOrganization();
                rp.setOrgName(name);
                rp.setOrgCode(code);
                rp.setOrgAddress(address);
                rp.setOrgPhone(phone);
                rp.setOrgTaxNo(taxNo);
                rp.setOrgDescription(description);
                rp.setOrgStatus(status);
                rp.setIpList(ipList);
                rp.setExpireDatetime(expired);
                rp.setCreateUser(principal.getName());
                rp.setOrgType(Byte.valueOf(orgType));
                rpOrganizationService.create(rp);
                List<String> serviceCodes = new ArrayList<>();
                List<RpOrgServiceRegister> sersNew = new ArrayList<>();
                if(!Strings.isNullOrEmpty(strService)) {
                    for(String str : strService.split(",")) {
                        RpService service = rpServiceService.findById(Integer.valueOf(str));
                        if(service != null) {
                            RpOrgServiceRegister one = new RpOrgServiceRegister();
                            one.setOrgId(rp);
                            one.setServiceId(service);
                            one.setRegisterStatus(Constans.ACTIVE_TXT);
                            one.setCreateUser(principal.getName());
                            rpOrgServiceRegisterService.create(one);
                            if(Strings.isNullOrEmpty(service.getCode())) {
                                serviceCodes.add(service.getCode());
                            }
                            sersNew.add(one);
                        }
                    }
                    rp.setRpOrgServiceRegisterCollection(sersNew);
                }
                String strDate = sdfApi.format(expired);
                List<String> ips = new ArrayList<>();
                for(String str : ipList.split(",")) {
                    ips.add(str);
                }

                HttpResponse<String> resp = Unirest.post(apiToken)
                        .header("Content-Type", "application/json")
                        .body(new JSONObject()
                                .put("requestUser", principal.getName())
                                .put("organizationCode", rp.getOrgCode())
                                .put("serviceCode", serviceCodes)
                                .put("ipAddress", ips)
                                .put("expireDate", strDate)
                                .toString())
                        .asString();
                if(resp.getStatus() == 200) {
                    rp.setToken(resp.getBody());
                } else {
                    LOG.error("{}", resp.getBody());
                }

                rpOrganizationService.create(rp);

                RpActionAudit actionAudit = new RpActionAudit();
                actionAudit.setUserAction(Constans.Audit.CREATE);
                actionAudit.setUserName(principal.getName());
                actionAudit.setObject("RpOrganization");
//                actionAudit.setDetail(rp.toString());

                RpActionAuditPK pk = new RpActionAuditPK();
                pk.setAuditId(new Random().nextInt(10));
                pk.setActionDatetime(new Date());
                actionAudit.setRpActionAuditPK(pk);
                rpActionAuditService.create(actionAudit);
            } else {
                model.addAttribute("message", message);
                return "organization/add";
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "redirect:/organization/list.html";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    public String addOrganizationGet(Model model) {
        LOG.info("===Add Organization===");
        List<RpService> services = rpServiceService.findByServiceStatus(Constans.ACTIVE_TXT);
        model.addAttribute("services", services);
        return "organization/add";
    }

    @RequestMapping(value = {"/delete.json"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    @ResponseBody
    public String deleteOrganization(@RequestParam(value="id" , defaultValue = "") Integer id, Principal principal) {
        LOG.info("===Delete RpOrganization===");
        RpOrganization rp = rpOrganizationService.findById(id);
        if(rp != null) {
            rpOrganizationService.delete(rp);
            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.DELETE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpOrganization");
//            actionAudit.setDetail(rp.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
        } else {
            return "ID not exist!!!";
        }
        return "OK";
    }

    @RequestMapping(value = {"/check_code.json"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    @ResponseBody
    public String checkCode(@RequestParam(value="code" , defaultValue = "") String code) {
        LOG.info("===Check Code===");
        RpOrganization rp = rpOrganizationService.findByOrgCode(code);
        if(rp != null) {
            return "NOK";
        } else {
            return "OK";
        }
    }

    @RequestMapping(value = {"/update.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    public String updateOrganiPost(Model model, Principal principal,
                                     @RequestParam(value="hidId" , defaultValue = "") Integer id,
                                     @RequestParam(value="strName" , defaultValue = "") String name,
                                     @RequestParam(value="strAdd" , defaultValue = "") String address,
                                     @RequestParam(value="strPhone" , defaultValue = "") String phone,
                                     @RequestParam(value="strTax" , defaultValue = "") String taxNo,
                                     @RequestParam(value="strDes" , defaultValue = "") String description,
                                     @RequestParam(value="strStt" , defaultValue = "") String status,
                                     @RequestParam(value="ipList" , defaultValue = "") String ipList,
                                     @RequestParam(value="expiredDate" , defaultValue = "") String expiredDate,
                                     @RequestParam(value="orgType" , defaultValue = "1") String orgType,
                                     @RequestParam(value="strService" , defaultValue = "1") String strService) {
        String message = "Error";
        RpOrganization rp = rpOrganizationService.findById(id);
        try {
            if(rp == null) {
                return "404";
            }
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(status)) {
                Date expired = sdf.parse(expiredDate);
                rp.setOrgName(name);
                rp.setOrgAddress(address);
                rp.setOrgPhone(phone);
                rp.setOrgTaxNo(taxNo);
                rp.setOrgDescription(description);
                rp.setOrgStatus(status);
                rp.setExpireDatetime(expired);
                rp.setIpList(ipList);
                rp.setUpdateUser(principal.getName());
                rp.setOrgType(Byte.valueOf(orgType));
                String strDate = sdfApi.format(expired);
                List<String> ips = new ArrayList<>();
                for(String str : ipList.split(",")) {
                    ips.add(str);
                }
                List<String> serviceCodesOld = new ArrayList<>();
                List<String> serviceCodesNew = new ArrayList<>();

                List<RpOrgServiceRegister> sers = rp.getRpOrgServiceRegisterCollection();
                List<RpOrgServiceRegister> sersNew = new ArrayList<>();
                for(RpOrgServiceRegister tmp : sers) {
                    serviceCodesOld.add(tmp.getServiceId().getCode());
                }
                if(!Strings.isNullOrEmpty(strService)) {
                    for(String str : strService.split(",")) {
                        RpService service = rpServiceService.findById(Integer.valueOf(str));
                        if(service != null) {
                            RpOrgServiceRegister one = new RpOrgServiceRegister();
                            one.setOrgId(rp);
                            one.setServiceId(service);
                            one.setRegisterStatus(Constans.ACTIVE_TXT);
                            one.setCreateUser(principal.getName());
//                            rpOrgServiceRegisterService.create(one);
                            if(Strings.isNullOrEmpty(service.getCode())) {
                                serviceCodesNew.add(service.getCode());
                                sersNew.add(one);
                            }
                        }
                    }
                    rp.setRpOrgServiceRegisterCollection(sersNew);
                }

                try {
                    HttpResponse<String> resp = Unirest.post(apiToken)
                            .header("Content-Type", "application/json")
                            .body(new JSONObject()
                                    .put("requestUser", principal.getName())
                                    .put("organizationCode", rp.getOrgCode())
                                    .put("serviceCode", serviceCodesNew)
                                    .put("ipAddress", ips)
                                    .put("expireDate", strDate)
                                    .toString())
                            .asString();
                    if (resp.getStatus() == 200) {
                        rp.setToken(resp.getBody());
                    } else {
                        LOG.error("{}", resp.getBody());
                    }
                } catch (Exception e) {
                    LOG.error("", e);
                }

                rpOrganizationService.update(rp);
            } else {
                model.addAttribute("message", message);
                model.addAttribute("rp", rp);
                return "organization/update";
            }

            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.UPDATE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpOrganization");
//            actionAudit.setDetail(rp.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
        } catch (Exception e) {
            LOG.error("", e);
            model.addAttribute("message", message);
            return "organization/update";
        }
        return "redirect:/organization/list.html";
    }

    @RequestMapping(value = {"/{id}/update.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String updateOrganiGet(Model model, Principal principal, @PathVariable("id")  Integer id) {
        LOG.info("===Update Organization===");
        RpOrganization rp = rpOrganizationService.findById(id);
        if(rp == null) {
            return "404";
        }
        List<RpService> services = rpServiceService.findByServiceStatus(Constans.ACTIVE_TXT);
        model.addAttribute("services", services);
        List<Integer> ownSers = new ArrayList<>();
        for(RpOrgServiceRegister tmp : rp.getRpOrgServiceRegisterCollection()) {
            ownSers.add(tmp.getServiceId().getServiceId());
        }
        model.addAttribute("ownSers", ownSers);
        model.addAttribute("rp", rp);
        model.addAttribute("id", id);
        return "organization/update";
    }

    @RequestMapping(value = {"/{id}/detail.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String detailOrgani(Model model, @PathVariable("id")  Integer id) {
        LOG.info("===Detail Organization===");
        RpOrganization rp = rpOrganizationService.findById(id);
        if(rp == null) {
            return "404";
        }
        List<RpProduct> products = rp.getRpProductCollection();
        String proName = "";
        for(RpProduct one : products) {
            proName += one.getProductName() + ", ";
        }
        model.addAttribute("products", products);
        model.addAttribute("proName", proName);
        model.addAttribute("rp", rp);
        model.addAttribute("id", id);
        return "organization/detail";
    }



    @RequestMapping(value = {"/{id}/add_service.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addServiceGet(Model model, Principal principal, @PathVariable("id")  Integer id,
                                     @RequestParam(value="result_message" , defaultValue = "") String resultMes,
                                     Pageable pageable) {
        LOG.info("===Add RpService Price===");
        RpOrganization rp = rpOrganizationService.findById(id);
        if(rp == null) {
            return "404";
        }
        Page<OrganiServiceRegisterModel> page;
        String not_found_message = "";

        List<RpService> services = rpServiceService.findByServiceStatus(Constans.ACTIVE_TXT);
        Pageable _pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize());
        Page<RpOrgServiceRegister> pageData = rpOrgServiceRegisterService.findList(rp, null, Constans.ACTIVE_TXT, _pageable);

        if (pageData.getContent().size() == 0) {
            not_found_message = Constans.NOT_FOUND_MESSAGE;
            model.addAttribute("page", pageData);
        } else {
            List<OrganiServiceRegisterModel> data = new ArrayList<>();
            for(RpOrgServiceRegister one : pageData.getContent()) {
                OrganiServiceRegisterModel tmp = new OrganiServiceRegisterModel();
                BeanUtils.copyProperties(one, tmp);
                RpService rpService = rpServiceService.findById(one.getServiceId().getServiceId());
                if(rpService != null) {
                    tmp.setServiceName(rpService.getServiceName());
                }
                data.add(tmp);
            }
            page = new PageImpl<>(data, pageable, pageData.getTotalElements());
            model.addAttribute("page", page);
        }

        model.addAttribute("result_message", resultMes);
        model.addAttribute("services", services);
        model.addAttribute("rp", rp);
        model.addAttribute("id", id);
        model.addAttribute("not_found_message", not_found_message);
        return "organization/add_service";
    }

    @RequestMapping(value = {"add_service.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addServicePost(Model model, Principal principal,
                                 @RequestParam(value="id" ) Integer id,
                                 @RequestParam(value="serviceId" , defaultValue = "") Integer serviceId,
                                 @RequestParam(value="strStt" , defaultValue = "") String strStt) {
        LOG.info("===Add RpService Price===");
        RpOrganization rp = rpOrganizationService.findById(id);
        if(rp == null) {
            return "404";
        }

        String not_found_message = "";

        List<RpService> services = rpServiceService.findByServiceStatus(Constans.ACTIVE_TXT);
        try {
            RpOrgServiceRegister one = new RpOrgServiceRegister();
            one.setOrgId(rp);
            RpService service = rpServiceService.findById(serviceId);
            one.setServiceId(service);
            one.setRegisterStatus(strStt);
            one.setCreateUser(principal.getName());
            rpOrgServiceRegisterService.create(one);

            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.CREATE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpOrgServiceRegister");
//            actionAudit.setDetail(one.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
        } catch (Exception e) {
            LOG.error("", e);
        }

        model.addAttribute("services", services);
        model.addAttribute("rp", rp);
        model.addAttribute("id", id);
        model.addAttribute("not_found_message", not_found_message);
        return "redirect:/organization/list.html";
    }

    @RequestMapping(value = {"/delete_service.json"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    @ResponseBody
    public String deleteServicePrice(@RequestParam(value="id" , defaultValue = "") Integer id, Principal principal) {
        LOG.info("===Delete RpService Price===");
        RpServicePrice rp = rpServicePriceService.findById(id);
        if(rp != null) {
            rpServicePriceService.delete(rp);
            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.DELETE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpServicePrice");
//            actionAudit.setDetail(rp.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
            return "OK";
        } else {
            return "NOK";
        }
    }

    @RequestMapping(value = {"/{id}/add_service_price.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addServicePriceGet(Model model, Principal principal, @PathVariable("id")  Integer id,
                                     @RequestParam(value="result_message" , defaultValue = "") String resultMes,
                                     Pageable pageable) {
        LOG.info("===Add RpService Price===");
        RpOrganization rp = rpOrganizationService.findById(id);
        if(rp == null) {
            return "404";
        }

        String not_found_message = "";

        Pageable _pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize());
        Page<RpServicePrice> pageData;
        Page<ServicePriceModel> page;

        pageData = rpServicePriceService.findList(rp, null, null, _pageable);

        if (pageData.getContent().size() == 0) {
            not_found_message = Constans.NOT_FOUND_MESSAGE;
            model.addAttribute("page", pageData);
        } else {
            List<ServicePriceModel> data = new ArrayList<>();
            for(RpServicePrice one : pageData.getContent()) {
                ServicePriceModel tmp = new ServicePriceModel();
                BeanUtils.copyProperties(one, tmp);
                RpService rpService = rpServiceService.findById(one.getServiceId().getServiceId());
                if(rpService != null) {
                    tmp.setServiceName(rpService.getServiceName());
                }
                data.add(tmp);
            }
            page = new PageImpl<>(data, pageable, pageData.getTotalElements());
            model.addAttribute("page", page);
        }
        List<RpService> services = new ArrayList<>();
        List<RpOrgServiceRegister> servicesUse = rp.getRpOrgServiceRegisterCollection();
        for(RpOrgServiceRegister one : servicesUse) {
            RpService tmp = rpServiceService.findById(one.getServiceId().getServiceId());
            services.add(tmp);
        }

        model.addAttribute("result_message", resultMes);
        model.addAttribute("services", services);
        model.addAttribute("rp", rp);
        model.addAttribute("id", id);
        model.addAttribute("not_found_message", not_found_message);
        return "organization/add_service_price";
    }

    @RequestMapping(value = {"/add_service_price.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addServicePricePost(Model model, Principal principal,
                                      @RequestParam(value="id" , defaultValue = "") Integer id,
                                      @RequestParam(value="serviceId" , defaultValue = "") Integer serviceId,
                                      @RequestParam(value="startDate" , defaultValue = "") String startDate,
                                      @RequestParam(value="endDate" , defaultValue = "") String endDate,
                                      @RequestParam(value="price" , defaultValue = "") String price,
                                      @RequestParam(value="regex" , defaultValue = "") String regex,
                                      @RequestParam(value="strStt" , defaultValue = "") String status,
                                      RedirectAttributes redirectAttributes) {
        LOG.info("===Add RpService Price Post===");
        RpOrganization rp = rpOrganizationService.findById(id);
        if(rp == null) {
            return "404";
        }
        try {
            price = price.replaceAll("[^0-9]", "");
            RpServicePrice one = new RpServicePrice();
            one.setOrgId(rp);
            RpService service = rpServiceService.findById(serviceId);
            one.setServiceId(service);
            one.setStartDatetime(sdf.parse(startDate));
            one.setEndDatetime(sdf.parse(endDate));
            one.setPrice(Double.valueOf(price));
            one.setRegex(regex);
            one.setPriceStatus(status);
            one.setCreateUser(principal.getName());
            rpServicePriceService.create(one);

            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.CREATE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpServicePrice");
//            actionAudit.setDetail(one.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
        } catch (Exception e) {
            LOG.error("", e);
            redirectAttributes.addAttribute("result_message", "Add RpService Price Error");
        }
        redirectAttributes.addAttribute("result_message", "Add RpService Price Success");
        String targetUrl = "organization/"+String.valueOf(id)+"/add_service_price.html";
        return "redirect:/"+targetUrl;
    }


}
