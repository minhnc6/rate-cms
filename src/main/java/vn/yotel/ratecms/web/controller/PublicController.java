package vn.yotel.ratecms.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value="/public")
public class PublicController {

    private static final Logger LOG = LoggerFactory.getLogger(PublicController.class);

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    HttpServletRequest request;

    private Authentication doAutoLogin(String username, String password, HttpServletRequest request) {
        Authentication authentication = null;
        try {
            // Must be called from request filtered by Spring Security, otherwise SecurityContextHolder is not updated
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            token.setDetails(new WebAuthenticationDetails(request));
            authentication = this.authenticationManager.authenticate(token);
            LOG.debug("Auto login with [{}]", authentication.getPrincipal());
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Create a new session and add the security context.
            HttpSession session = request.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", SecurityContextHolder.getContext());
        } catch (Exception e) {
            SecurityContextHolder.getContext().setAuthentication(null);
            LOG.debug("Failure in autoLogin", e);
        }
        return authentication;
    }

    @RequestMapping(value="/login.json", method = { RequestMethod.GET , RequestMethod.POST })
    @ResponseBody
    public String login(Model model
            , @RequestParam(value = "txtUsername" ) String txtUsername
            , @RequestParam(value = "txtPassword" ) String txtPassword
            , @RequestParam(value = "login", defaultValue = "0") int login){
        Authentication authentication = null;
        if(txtUsername != null && !txtUsername.isEmpty()){
            authentication = doAutoLogin(txtUsername, txtPassword, request);
            if(authentication == null){
                return "NOK";
            }
        }
        return "OK";
    }
}
