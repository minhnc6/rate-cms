package vn.yotel.ratecms.web.controller;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import vn.yotel.commons.util.RestMessage;
import vn.yotel.ratecms.Constans;
import vn.yotel.ratecms.jpa.RpActionAudit;
import vn.yotel.ratecms.jpa.RpActionAuditPK;
import vn.yotel.ratecms.jpa.RpMessage;
import vn.yotel.ratecms.service.RpActionAuditService;
import vn.yotel.ratecms.service.RpMessageService;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/message")
public class RpMessageController {

    private static final Logger LOG = LoggerFactory.getLogger(RpMessageController.class);

    @Autowired
    RpMessageService rpMessageService;

    @Autowired
    RpActionAuditService rpActionAuditService;

    @RequestMapping(value = {"/","/list","/list.html"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String listMessage(Model model,
                               @RequestParam(value="strName" , defaultValue = "") String name,
                               @RequestParam(value="strStt" , defaultValue = "") String status) {
        try {
            if(Strings.isNullOrEmpty(name)) {
                name = null;
            }
            List<RpMessage> result;
            if(Strings.isNullOrEmpty(status)) {
                result = rpMessageService.findList(name, null);
            } else {
                result = rpMessageService.findList(name, status);
            }
            model.addAttribute("result", result);
            model.addAttribute("name", name);
            model.addAttribute("status", status);
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "message/list";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    public String addMessagePost(Model model,
                                  Principal principal,
                                  @RequestParam(value="strName" , defaultValue = "") String name,
                                  @RequestParam(value="strContent" , defaultValue = "") String content,
                                  @RequestParam(value="strDes" , defaultValue = "") String description,
                                  @RequestParam(value="strStt" , defaultValue = "") String status) {
        String message = "Error";
        try {
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(content)
                    && !Strings.isNullOrEmpty(status)
                    && !Strings.isNullOrEmpty(description)) {
                RpMessage rp = new RpMessage();
                rp.setMsgName(name);
                rp.setContent(content);
                rp.setMsgDescription(description);
                rp.setMsgStatus(status);
                rp.setCreateUser(principal.getName());
                rpMessageService.create(rp);

                RpActionAudit actionAudit = new RpActionAudit();
                actionAudit.setUserAction(Constans.Audit.CREATE);
                actionAudit.setUserName(principal.getName());
                actionAudit.setObject("RpMessage");
//                actionAudit.setDetail(rp.toString());

                RpActionAuditPK pk = new RpActionAuditPK();
                pk.setAuditId(new Random().nextInt(10));
                actionAudit.setRpActionAuditPK(pk);
                rpActionAuditService.create(actionAudit);
            } else {
                model.addAttribute("message", message);
                return "message/add";
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "redirect:/message/list.html";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    public String addResourceGet(Model model, Principal principal) {
        LOG.info("===Add Resource===");
        return "message/add";
    }

    @RequestMapping(value = {"/update.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    public String updateResourcePost(Model model, Principal principal,
                                     @RequestParam(value="hidId" , defaultValue = "") Integer id,
                                     @RequestParam(value="strName" , defaultValue = "") String name,
                                     @RequestParam(value="strContent" , defaultValue = "") String content,
                                     @RequestParam(value="strDes" , defaultValue = "") String description,
                                     @RequestParam(value="strStt" , defaultValue = "") String status) {
        String message = "Error";
        try {
            RpMessage rp = rpMessageService.findById(id);
            if(rp == null) {
                return "404";
            }
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(content)
                    && !Strings.isNullOrEmpty(description)
                    && !Strings.isNullOrEmpty(status)) {
                rp.setMsgName(name);
                rp.setContent(content);
                rp.setMsgDescription(description);
                rp.setMsgStatus(status);
                rp.setUpdateUser(principal.getName());
                rpMessageService.update(rp);
            } else {
                model.addAttribute("message", message);
                return "message/update";
            }
            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.UPDATE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpMessage");
//            actionAudit.setDetail(rp.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "redirect:/message/list.html";
    }

    @RequestMapping(value = {"/{id}/update.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    public String updateResourceGet(Model model, Principal principal, @PathVariable("id")  Integer id) {
        LOG.info("===Update Resource===");
        RpMessage rp = rpMessageService.findById(id);
        if(rp == null) {
            return "404";
        }
        model.addAttribute("rp", rp);
        model.addAttribute("id", id);
        return "message/update";
    }

    @RequestMapping(value = {"/delete.json"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    @ResponseBody
    public String deleteResource(@RequestParam(value="id" , defaultValue = "") Integer id, Principal principal) {
        LOG.info("===Delete Resource===");
        RpMessage rp = rpMessageService.findById(id);
        if(rp != null) {
            rpMessageService.delete(rp);
        } else {
            return "ID not exist!!!";
        }
        RpActionAudit actionAudit = new RpActionAudit();
        actionAudit.setUserAction(Constans.Audit.DELETE);
        actionAudit.setUserName(principal.getName());
        actionAudit.setObject("RpMessage");
//        actionAudit.setDetail(rp.toString());

        RpActionAuditPK pk = new RpActionAuditPK();
        pk.setAuditId(new Random().nextInt(10));
        pk.setActionDatetime(new Date());
        actionAudit.setRpActionAuditPK(pk);
        rpActionAuditService.create(actionAudit);
        return "OK";
    }

    @RequestMapping(value = {"/list.json"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    @ResponseBody
    public RestMessage listResourceJson(Model model,
                                        @RequestParam(value="strName" , defaultValue = "") String name,
                                        @RequestParam(value="strStt" , defaultValue = "") String status) {
        RestMessage mes;
        try {
            if(Strings.isNullOrEmpty(name)) {
                name = null;
            }
            List<RpMessage> result;
            if(Strings.isNullOrEmpty(status)) {
                result = rpMessageService.findList(name, null);
            } else {
                result = rpMessageService.findList(name, status);
            }
            mes = RestMessage.RestMessageBuilder.SUCCESS();
            mes.setData(result);
        } catch (Exception e) {
            LOG.error("", e);
            mes = RestMessage.RestMessageBuilder.FAIL("1", "");
        }
        return mes;
    }
}
