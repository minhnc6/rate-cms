package vn.yotel.ratecms.web.controller;

import com.google.common.base.Strings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import vn.yotel.commons.util.Util;
import vn.yotel.ratecms.Constans;
import vn.yotel.ratecms.jpa.RpActionAudit;
import vn.yotel.ratecms.service.RpActionAuditService;

import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/audit")
public class RpActionAuditController {

    private static final Logger LOG = LoggerFactory.getLogger(RpActionAuditController.class);

    @Autowired
    RpActionAuditService rpActionAuditService;

    @RequestMapping(value = "/list.html", method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String dailyResult(Model model,
                              HttpSession session,
                              @RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                              @RequestParam(value = "toDate", defaultValue = "") String toDate,
                              @RequestParam(value = "userName", defaultValue = "") String userName,
                              @RequestParam(value = "userAction", defaultValue = "") String userAction,
                              @RequestParam(value = "obj", defaultValue = "") String _object,
                              Pageable pageable) {
        String not_found_message = "";

        //datetime
        DateTime dateTime = new DateTime();
        Date _fromDate = dateTime.withTimeAtStartOfDay().toDate();
        Date _toDate = dateTime.withTime(23, 59, 59, 999).toDate();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            _fromDate = dateFormat.parse(fromDate);
            _toDate = dateFormat.parse(toDate);
        } catch (Exception ex) {
        }
        model.addAttribute("fromDate", dateFormat.format(_fromDate));
        model.addAttribute("toDate", dateFormat.format(_toDate));

        Pageable _pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize());

        if (Strings.isNullOrEmpty(userName)) {
            userName = null;
        }
        if (Strings.isNullOrEmpty(userAction)) {
            userAction = null;
        }
        if (Strings.isNullOrEmpty(_object)) {
            _object = null;
        }

        Page<RpActionAudit> pageData = rpActionAuditService.findList(_fromDate, _toDate, userName, userAction, _object, _pageable);
        if (pageData.getContent().size() == 0) {
            not_found_message = Constans.NOT_FOUND_MESSAGE;
        }
        model.addAttribute("page", pageData);
        model.addAttribute("userName", userName);
        model.addAttribute("userAction", userAction);
        model.addAttribute("_object", _object);
        model.addAttribute("not_found_message", not_found_message);
        return "audit/list";
    }
}
