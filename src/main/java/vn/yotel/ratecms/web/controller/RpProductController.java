package vn.yotel.ratecms.web.controller;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import vn.yotel.ratecms.Constans;
import vn.yotel.ratecms.jpa.RpActionAudit;
import vn.yotel.ratecms.jpa.RpActionAuditPK;
import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.jpa.RpProduct;
import vn.yotel.ratecms.service.RpActionAuditService;
import vn.yotel.ratecms.service.RpOrganizationService;
import vn.yotel.ratecms.service.RpProductService;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Date;
import java.util.Random;

@Controller
@RequestMapping("/product")
public class RpProductController {
    private static final Logger LOG = LoggerFactory.getLogger(RpProductController.class);

    @Autowired
    RpProductService rpProductService;

    @Autowired
    RpOrganizationService rpOrganizationService;

    @Autowired
    RpActionAuditService rpActionAuditService;

    @RequestMapping(value = "/list.html", method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String list(Model model,
                              @RequestParam(value = "strName", defaultValue = "") String name,
                              @RequestParam(value="strStt" , defaultValue = "") String status,
                              Pageable pageable) {
        String not_found_message = "";

        Pageable _pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize());
        Page<RpProduct> pageData;
        if (Strings.isNullOrEmpty(name)) {
            name = null;
        }
        if(Strings.isNullOrEmpty(status)) {
            status = null;
            pageData = rpProductService.findList(name, null, _pageable);
        } else {
            pageData = rpProductService.findList(name, status, _pageable);
        }

        if (pageData.getContent().size() == 0) {
            not_found_message = Constans.NOT_FOUND_MESSAGE;
        }
        model.addAttribute("page", pageData);
        model.addAttribute("name", name);
        model.addAttribute("status", status);
        model.addAttribute("not_found_message", not_found_message);
        return "product/list";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addProductPost(Model model,
                                 Principal principal,
                                 @RequestParam(value="orgId" , defaultValue = "") Integer orgId,
                                 @RequestParam(value="strName" , defaultValue = "") String name,
                                 @RequestParam(value="strDes" , defaultValue = "") String description,
                                 @RequestParam(value="strStt" , defaultValue = "") String status) {
        String message = "Error";
        try {
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(status)
                    && !Strings.isNullOrEmpty(description)) {
                RpOrganization org = rpOrganizationService.findById(orgId);
                RpProduct rp = new RpProduct();
                rp.setOrgId(org);
                rp.setProductName(name);
                rp.setProductDescription(description);
                rp.setProductStatus(status);
                rpProductService.create(rp);

                RpActionAudit actionAudit = new RpActionAudit();
                actionAudit.setUserAction(Constans.Audit.CREATE);
                actionAudit.setUserName(principal.getName());
                actionAudit.setObject("RpProduct");
//                actionAudit.setDetail(rp.toString());

                RpActionAuditPK pk = new RpActionAuditPK();
                pk.setAuditId(new Random().nextInt(10));
                pk.setActionDatetime(new Date());
                actionAudit.setRpActionAuditPK(pk);
                rpActionAuditService.create(actionAudit);
            } else {
                model.addAttribute("message", message);
                return "product/add";
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "redirect:/product/list.html";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addProductGet(Model model, Principal principal, @RequestParam(value="orgId" , defaultValue = "") Integer orgId) {
        LOG.info("===Add RpProduct===");
        RpOrganization rp = rpOrganizationService.findById(orgId);

        model.addAttribute("orgName", rp.getOrgName());
        model.addAttribute("orgId", orgId);
        return "product/add";
    }

    @RequestMapping(value = {"/delete.json"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    @ResponseBody
    public String deleteResource(@RequestParam(value="id" , defaultValue = "") Integer id, Principal principal) {
        LOG.info("===Delete RpProduct===");
        RpProduct rp = rpProductService.findById(id);
        if(rp != null) {
            rpProductService.delete(rp);

            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.DELETE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpProduct");
//            actionAudit.setDetail(rp.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
        } else {
            return "ID not exist!!!";
        }
        return "OK";
    }
}
