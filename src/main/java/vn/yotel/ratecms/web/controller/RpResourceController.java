package vn.yotel.ratecms.web.controller;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import vn.yotel.ratecms.Constans;
import vn.yotel.ratecms.jpa.RpActionAudit;
import vn.yotel.ratecms.jpa.RpActionAuditPK;
import vn.yotel.ratecms.jpa.RpResource;
import vn.yotel.ratecms.service.ResourceTblService;
import vn.yotel.ratecms.service.RpActionAuditService;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/res")
public class RpResourceController {

    private static final Logger LOG = LoggerFactory.getLogger(RpResourceController.class);

    @Autowired
    ResourceTblService rpResourceTblService;

    @Autowired
    RpActionAuditService rpActionAuditService;

    @RequestMapping(value = {"/","/list","/list.html"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String listResource(Model model,
                               @RequestParam(value="strName" , defaultValue = "") String name,
                               @RequestParam(value="strStt" , defaultValue = "") String status) {
        try {
            if(Strings.isNullOrEmpty(name)) {
                name = null;
            }
            List<RpResource> result;
            if(Strings.isNullOrEmpty(status)) {
                result = rpResourceTblService.findList(name, null);
            } else {
                result = rpResourceTblService.findList(name, status);
            }
            model.addAttribute("result", result);
            model.addAttribute("name", name);
            model.addAttribute("status", status);
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "res/list";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addResourcePost(Model model,
                               Principal principal,
                               @RequestParam(value="strName" , defaultValue = "") String name,
                               @RequestParam(value="strUrl" , defaultValue = "") String url,
                               @RequestParam(value="strDes" , defaultValue = "") String des,
                               @RequestParam(value="strStt" , defaultValue = "") String status) {
        String message = "Error";
        try {
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(url)
                    && !Strings.isNullOrEmpty(status)) {
                RpResource rp = new RpResource();
                rp.setResourceName(name);
                rp.setResourceStatus(status);
                rp.setUrl(url);
                rp.setResourceDescription(des);
                rp.setCreateUser(principal.getName());
                rpResourceTblService.create(rp);

                RpActionAudit actionAudit = new RpActionAudit();
                actionAudit.setUserAction(Constans.Audit.CREATE);
                actionAudit.setUserName(principal.getName());
                actionAudit.setObject("RpResource");
//                actionAudit.setDetail(rp.toString());

                RpActionAuditPK pk = new RpActionAuditPK();
                pk.setAuditId(new Random().nextInt(10));
                pk.setActionDatetime(new Date());
                actionAudit.setRpActionAuditPK(pk);
                rpActionAuditService.create(actionAudit);
            } else {
                model.addAttribute("message", message);
                return "res/add";
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "redirect:/res/list.html";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addResourceGet(Model model, Principal principal) {
        LOG.info("===Add Resource===");
        return "res/add";
    }

    @RequestMapping(value = {"/update.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    public String updateResourcePost(Model model, Principal principal,
                                  @RequestParam(value="hidId" , defaultValue = "") Integer id,
                                  @RequestParam(value="strName" , defaultValue = "") String name,
                                  @RequestParam(value="strUrl" , defaultValue = "") String url,
                                  @RequestParam(value="strStt" , defaultValue = "") String status) {
        String message = "Error";
        try {
            RpResource rp = rpResourceTblService.findById(id);
            if(rp == null) {
                return "404";
            }
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(url)
                    && !Strings.isNullOrEmpty(status)) {
                rp.setResourceName(name);
                rp.setResourceStatus(status);
                rp.setUrl(url);
                rp.setUpdateUser(principal.getName());
                rpResourceTblService.update(rp);

                RpActionAudit actionAudit = new RpActionAudit();
                actionAudit.setUserAction(Constans.Audit.UPDATE);
                actionAudit.setUserName(principal.getName());
                actionAudit.setObject("RpResource");
//                actionAudit.setDetail(rp.toString());

                RpActionAuditPK pk = new RpActionAuditPK();
                pk.setAuditId(new Random().nextInt(10));
                pk.setActionDatetime(new Date());
                actionAudit.setRpActionAuditPK(pk);
                rpActionAuditService.create(actionAudit);
            } else {
                model.addAttribute("message", message);
                return "res/update";
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "redirect:/res/list.html";
    }

    @RequestMapping(value = {"/{id}/update.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String updateResourceGet(Model model, Principal principal, @PathVariable("id") Integer id) {
        LOG.info("===Update Resource===");
        RpResource rp = rpResourceTblService.findById(id);
        if(rp == null) {
            return "404";
        }
        model.addAttribute("rp", rp);
        model.addAttribute("id", id);
        return "res/update";
    }

    @RequestMapping(value = {"/delete.json"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    @ResponseBody
    public String deleteResource(@RequestParam(value = "id", defaultValue = "") Integer id, Principal principal) {
        LOG.info("===Delete Resource===");
        RpResource rp = rpResourceTblService.findById(id);
        if(rp != null) {
            rpResourceTblService.delete(rp);

            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.DELETE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpResource");
//            actionAudit.setDetail(rp.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
        } else {
            return "ID not exist!!!";
        }
        return "OK";
    }

}
