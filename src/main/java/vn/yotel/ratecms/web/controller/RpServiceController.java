package vn.yotel.ratecms.web.controller;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import vn.yotel.ratecms.Constans;
import vn.yotel.ratecms.jpa.RpActionAudit;
import vn.yotel.ratecms.jpa.RpActionAuditPK;
import vn.yotel.ratecms.jpa.RpResource;
import vn.yotel.ratecms.jpa.RpService;
import vn.yotel.ratecms.service.ResourceTblService;
import vn.yotel.ratecms.service.RpActionAuditService;
import vn.yotel.ratecms.service.RpServiceService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/service")
public class RpServiceController {

    private static final Logger LOG = LoggerFactory.getLogger(RpServiceController.class);

    @Autowired
    RpServiceService rpServiceService;

    @Autowired
    ResourceTblService rpResourceTblService;

    @Autowired
    RpActionAuditService rpActionAuditService;

    @RequestMapping(value = {"/","/list","/list.html"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String listService(Model model,
                               @RequestParam(value="strName" , defaultValue = "") String name,
                               @RequestParam(value="strStt" , defaultValue = "") String status) {
        try {
            if(Strings.isNullOrEmpty(name)) {
                name = null;
            }
            List<RpService> result;
            if(Strings.isNullOrEmpty(status)) {
                result = rpServiceService.findList(name, null);
            } else {
                result = rpServiceService.findList(name, status);
            }
            model.addAttribute("result", result);
            model.addAttribute("name", name);
            model.addAttribute("status", status);
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "service/list";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addServicePost(Model model,
                                  Principal principal,
                                  @RequestParam(value="strName" , defaultValue = "") String name,
                                  @RequestParam(value="strDes" , defaultValue = "") String description,
                                  @RequestParam(value="strRes" , defaultValue = "") String resource,
                                  @RequestParam(value="strStt" , defaultValue = "") String status) {
        String message = "Error";
        try {
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(description)
                    && !Strings.isNullOrEmpty(status)) {
                RpService rp = new RpService();
                rp.setServiceName(name);
                rp.setServiceStatus(status);
                rp.setServiceDescription(description);
                List<RpResource> res = new ArrayList<>();
                for(String str : resource.split(",")) {
                    RpResource one = rpResourceTblService.findById(Integer.valueOf(str));
                    res.add(one);
                }
                rp.setRpResourceList(res);
                rp.setCreateUser(principal.getName());
                rpServiceService.create(rp);

                RpActionAudit actionAudit = new RpActionAudit();
                actionAudit.setUserAction(Constans.Audit.CREATE);
                actionAudit.setUserName(principal.getName());
                actionAudit.setObject("RpService");
//                actionAudit.setDetail(rp.toString());

                RpActionAuditPK pk = new RpActionAuditPK();
                pk.setAuditId(new Random().nextInt(10));
                pk.setActionDatetime(new Date());
                actionAudit.setRpActionAuditPK(pk);
                rpActionAuditService.create(actionAudit);
            } else {
                List<RpResource> lstResource = rpResourceTblService.findByResourceStatus(Constans.ACTIVE_TXT);

                model.addAttribute("lstResource", lstResource);
                model.addAttribute("message", message);
                return "service/add";
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "redirect:/service/list.html";
    }

    @RequestMapping(value = {"/add.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String addResourceGet(Model model, Principal principal) {
        LOG.info("===Add RpService===");
        List<RpResource> lstResource = rpResourceTblService.findByResourceStatus(Constans.ACTIVE_TXT);

        model.addAttribute("lstResource", lstResource);
        return "service/add";
    }

    @RequestMapping(value = {"/update.html"}, method = {RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String updateResourcePost(Model model, Principal principal,
                                     @RequestParam(value="hidId" , defaultValue = "") Integer id,
                                     @RequestParam(value="strName" , defaultValue = "") String name,
                                     @RequestParam(value="strDes" , defaultValue = "") String description,
                                     @RequestParam(value="strRes" , defaultValue = "") String resource,
                                     @RequestParam(value="strStt" , defaultValue = "") String status) {
        String message = "Error";
        try {
            RpService rp = rpServiceService.findById(id);
            if(rp == null) {
                return "404";
            }
            if(!Strings.isNullOrEmpty(name)
                    && !Strings.isNullOrEmpty(resource)
                    && !Strings.isNullOrEmpty(status)) {
                rp.setServiceName(name);
                rp.setServiceStatus(status);
                rp.setServiceDescription(description);
                List<RpResource> res = new ArrayList<>();
                for(String str : resource.split(",")) {
                    RpResource one = rpResourceTblService.findById(Integer.valueOf(str));
                    res.add(one);
                }
                rp.setRpResourceList(res);
                rp.setUpdateUser(principal.getName());
                rpServiceService.update(rp);

                RpActionAudit actionAudit = new RpActionAudit();
                actionAudit.setUserAction(Constans.Audit.UPDATE);
                actionAudit.setUserName(principal.getName());
                actionAudit.setObject("RpService");
//                actionAudit.setDetail(rp.toString());

                RpActionAuditPK pk = new RpActionAuditPK();
                pk.setAuditId(new Random().nextInt(10));
                pk.setActionDatetime(new Date());
                actionAudit.setRpActionAuditPK(pk);
                rpActionAuditService.create(actionAudit);
            } else {
                List<RpResource> lstResource = rpResourceTblService.findByResourceStatus(Constans.ACTIVE_TXT);

                model.addAttribute("lstResource", lstResource);
                model.addAttribute("message", message);
                return "service/update";
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "redirect:/service/list.html";
    }

    @RequestMapping(value = {"/{id}/update.html"}, method = {RequestMethod.GET})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    public String updateResourceGet(Model model, Principal principal, @PathVariable("id")  Integer id) {
        LOG.info("===Update RpService===");
        RpService rp = rpServiceService.findById(id);
        List<Integer> ownRes = new ArrayList<>();
        List<RpResource> resource = rp.getRpResourceList();
        for(RpResource one : resource) {
            ownRes.add(one.getResourceId());
        }
        if(rp == null) {
            return "404";
        }
        List<RpResource> lstResource = rpResourceTblService.findByResourceStatus(Constans.ACTIVE_TXT);

        model.addAttribute("lstResource", lstResource);
        model.addAttribute("ownRes", ownRes);
        model.addAttribute("rp", rp);
        model.addAttribute("id", id);
        return "service/update";
    }

    @RequestMapping(value = {"/delete.json"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators')")
    @ResponseBody
    public String deleteService(@RequestParam(value="id" , defaultValue = "") Integer id, Principal principal) {
        LOG.info("===Delete RpService===");
        RpService rp = rpServiceService.findById(id);
        if(rp != null) {
            rpServiceService.delete(rp);

            RpActionAudit actionAudit = new RpActionAudit();
            actionAudit.setUserAction(Constans.Audit.DELETE);
            actionAudit.setUserName(principal.getName());
            actionAudit.setObject("RpService");
//            actionAudit.setDetail(rp.toString());

            RpActionAuditPK pk = new RpActionAuditPK();
            pk.setAuditId(new Random().nextInt(10));
            pk.setActionDatetime(new Date());
            actionAudit.setRpActionAuditPK(pk);
            rpActionAuditService.create(actionAudit);
        } else {
            return "ID not exist!!!";
        }
        return "OK";
    }

}
