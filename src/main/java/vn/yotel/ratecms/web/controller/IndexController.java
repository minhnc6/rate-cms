package vn.yotel.ratecms.web.controller;

import com.google.common.base.Strings;
import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import vn.yotel.commons.util.Util;

import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class IndexController {

    @RequestMapping(value = {"/","/index","/index.html","/report/dashboard"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers','CustCare','Users')")
    public String listChargeLog(Model model,
                                HttpSession session,
                                @RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                                @RequestParam(value = "toDate", defaultValue = "") String toDate) {
        //datetime
        DateTime dateTime = new DateTime();
        Date _fromDate = dateTime.withTimeAtStartOfDay().toDate();
        Date _toDate = dateTime.withTime(23, 59, 59, 999).toDate();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            _fromDate = dateFormat.parse(fromDate);
            _toDate = dateFormat.parse(toDate);
        } catch (ParseException ex) {
        }
        model.addAttribute("fromDate", dateFormat.format(_fromDate));
        model.addAttribute("toDate", dateFormat.format(_toDate));
        return "report/dashboard";
    }
}
