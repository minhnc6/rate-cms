package vn.yotel.ratecms.web.controller;

import com.google.common.base.Strings;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import vn.yotel.admin.jpa.AuthUser;
import vn.yotel.admin.service.AuthUserService;
import vn.yotel.commons.util.Util;
import vn.yotel.ratecms.Constans;
import vn.yotel.ratecms.jpa.*;
import vn.yotel.ratecms.model.CampaignFullModel;
import vn.yotel.ratecms.model.CampaignModel;
import vn.yotel.ratecms.model.ReportFullApiRequestModel;
import vn.yotel.ratecms.repository.UserOrgRepo;
import vn.yotel.ratecms.service.*;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/report")
public class ReportController {

    private static final Logger LOG = LoggerFactory.getLogger(ReportController.class);

    @Autowired
    RpCampaignDailyResultService rpCampaignDailyResultService;

    @Autowired
    RpProductService rpProductService;

    @Autowired
    RpMessageService rpMessageService;

    @Autowired
    RpOrganizationService rpOrganizationService;

    @Autowired
    ApiRequestLogService apiRequestLogService;

    @Autowired
    UserOrgRepo userOrgRepo;

    @Autowired
    private AuthUserService authUserService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @RequestMapping(value = "/daily_result.html", method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Managers')")
    public String dailyResult(Model model,
                              HttpSession session,
                              @RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                              @RequestParam(value = "toDate", defaultValue = "") String toDate,
                              @RequestParam(value = "msisdn", defaultValue = "") String msisdn,
                              @RequestParam(value = "campaignId", required = false) Integer campaignId,
                              @RequestParam(value = "productId", required = false) Integer productId,
                              @RequestParam(value = "msgId", required = false) Integer msgId,
                              @RequestParam(value = "orgId", required = false) Integer orgId,
                              Pageable pageable) {
        String not_found_message = "";
        //datetime
        DateTime dateTime = new DateTime();
        Date _fromDate = dateTime.withTimeAtStartOfDay().toDate();
        Date _toDate = dateTime.withTime(23, 59, 59, 999).toDate();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            _fromDate = dateFormat.parse(fromDate);
            _toDate = dateFormat.parse(toDate);
        } catch (Exception ex) {
        }
        model.addAttribute("fromDate", dateFormat.format(_fromDate));
        model.addAttribute("toDate", dateFormat.format(_toDate));

//        Sort sort = new Sort(new Sort.Order(Sort.Direction.DESC, "createdDate"));
        Pageable _pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize());
        String _msisdn = msisdn;
        if (Strings.isNullOrEmpty(_msisdn)) {
            _msisdn = null;
        } else {
            _msisdn = Util.normalizeMsIsdn(_msisdn);
        }
        String campaignName = "", productName = "", messageName = "", organizationName = "";
        Page<CampaignDailyResult> pageData = rpCampaignDailyResultService.findList(_fromDate, _toDate, _msisdn, orgId, productId, msgId, campaignId, _pageable);
        if (pageData.getContent().size() == 0) {
            not_found_message = Constans.NOT_FOUND_MESSAGE;
        }
        List<RpProduct> products = rpProductService.findByProductStatus(Constans.ACTIVE_TXT);
        List<RpMessage> messages = rpMessageService.findByMsgStatus(Constans.ACTIVE_TXT);
        List<RpOrganization> organizations = rpOrganizationService.findByOrgStatus(Constans.ACTIVE_TXT);
        List<Campaign> campaigns = rpCampaignDailyResultService.findCampaigns(Constans.ACTIVE_TXT);

        model.addAttribute("page", pageData);
        model.addAttribute("msisdn", _msisdn);
        model.addAttribute("products", products);
        model.addAttribute("messages", messages);
        model.addAttribute("organizations", organizations);
        model.addAttribute("campaigns", campaigns);

        model.addAttribute("campaignId", campaignId);
        model.addAttribute("productId", productId);
        model.addAttribute("msgId", msgId);
        model.addAttribute("orgId", orgId);
        model.addAttribute("not_found_message", not_found_message);

        return "report/daily_result";
    }

    @RequestMapping(value = "/doisoat_chitiet.html", method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Users')")
    public String chitiet(Model model,
                          HttpSession session,
                          @RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                          @RequestParam(value = "toDate", defaultValue = "") String toDate,
                          @RequestParam(value = "orgId", defaultValue = "") String orgId,
                          Principal principal) {
        try {
            String not_found_message = "", not_found_message2 = "";
            //datetime
            DateTime dateTime = new DateTime();
            Date _fromDate = dateTime.withTimeAtStartOfDay().toDate();
            Date _toDate = dateTime.withTime(23, 59, 59, 999).toDate();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            try {
                _fromDate = dateFormat.parse(fromDate);
                _toDate = dateFormat.parse(toDate);
            } catch (Exception ex) {
            }
            model.addAttribute("fromDate", dateFormat.format(_fromDate));
            model.addAttribute("toDate", dateFormat.format(_toDate));

            Map<Integer, String> mapCamp = new HashMap<>();
            List<Campaign> allCamp = rpCampaignDailyResultService.findAllCamp();
            for (Campaign one : allCamp) {
                mapCamp.put(one.getCampaignId(), one.getCampaignCode());
            }
            Map<Integer, String> mapOrg = new HashMap<>();
            List<RpOrganization> allOrg;
            // lay org id tu username -- start
            Integer orgIdInt = getOrgIdFromUserName(principal.getName());
            RpOrganization orgCurrent = null;
            if (orgIdInt != null) {
                allOrg = new ArrayList<>();
                orgCurrent = rpOrganizationService.findById(orgIdInt);
                allOrg.add(orgCurrent);
            } else {
                allOrg = rpOrganizationService.findAll();
            }

            for (RpOrganization one : allOrg) {
                mapOrg.put(one.getOrgId(), one.getOrgName());
            }
            // lay org id tu username -- end

            List<CampaignModel> data2 = new ArrayList<>();
            List<CampaignDailyResult> data;
            List<RpDetailInformation> data1 = new ArrayList<>();

            List<Object[]> data3;

            if (orgIdInt == null) {
                if (Strings.isNullOrEmpty(orgId)) {
                    data = rpCampaignDailyResultService.findList(_fromDate, _toDate, null, null, null, null, null);
                    data3 = apiRequestLogService.findByRequestDatetimeNew(_fromDate, _toDate, null);
                } else {
                    data = rpCampaignDailyResultService.findList(_fromDate, _toDate, null, Integer.valueOf(orgId), null, null, null);
                    RpOrganization org = rpOrganizationService.findById(Integer.valueOf(orgId));
                    if (org != null) {
                        data3 = apiRequestLogService.findByRequestDatetimeNew(_fromDate, _toDate, org.getOrgCode());
                    } else {
                        data3 = apiRequestLogService.findByRequestDatetimeNew(_fromDate, _toDate, null);
                    }
                }
            } else {
                data = rpCampaignDailyResultService.findList(_fromDate, _toDate, null, orgIdInt, null, null, null);
                if (orgCurrent != null) {
                    data3 = apiRequestLogService.findByRequestDatetimeNew(_fromDate, _toDate, orgCurrent.getOrgCode());
                } else {
                    data3 = apiRequestLogService.findByRequestDatetimeNew(_fromDate, _toDate, null);
                }
            }

            for (CampaignDailyResult tmp : data) {
                CampaignModel one = new CampaignModel();
                one.setIsdn(tmp.getIsdn());
                one.setReceiveDatetime(tmp.getCampaignDailyResultPK().getReceiveDatetime());
                if (tmp.getCampaignId() != null) {
                    one.setCampaignName(mapCamp.get(tmp.getCampaignId()));
                }
                if (tmp.getOrgId() != null) {
                    one.setOrgName(mapOrg.get(tmp.getOrgId()));
                }
                data2.add(one);
            }
            if (!CollectionUtils.isEmpty(data3)){
                for (Object[] obj : data3) {
                    RpDetailInformation one = new RpDetailInformation();
                    one.setIsdn(obj[0].toString());
                    one.setService(obj[1].toString());
                    one.setRequestDate(obj[2] == null ? null :obj[2].toString());
                    one.setOrgCode(obj[3] == null ? null : obj[3].toString());
                    one.setRequestStatus(obj[4] == null ? null : obj[4].toString());
                    one.setResponseStatus(obj[5] == null ? null : obj[5].toString());
                    one.setResponseDate(obj[6]==null?null:obj[6].toString());
                    one.setTransId(obj[7] == null ? null : obj[7].toString());
                    one.setStatus(obj[8] == null ? null : obj[8].toString());
                    one.setReqRequest(obj[9] == null ? null : obj[9].toString());
                    one.setReqResponse(obj[10] == null ? null : obj[10].toString());
                    one.setResRequest(obj[11] == null ? null : obj[11].toString());
                    one.setResResponse(obj[12] == null ? null : obj[12].toString());
                    data1.add(one);
                }
            }

//            for (ApiRequestLog one : data1) {
//                if (one.getServiceCode().toLowerCase().contains(Constans.ServiceCode.SCORING)) {
//                    if ("0".equals(one.getErrorCode())) {
//                        if ("0".equals(one.getStatus())) {
//                            one.setResult("Thành công");
//                        } else {
//                            one.setResult("Thất bại");
//                        }
//                    } else {
//                        one.setResult("Lỗi hệ thống");
//                    }
//                    one.setType("Điểm");
//                } else {
//                    if ("0".equals(one.getErrorCode())) {
//                        one.setResult("Thành công");
//                    } else {
//                        one.setResult("Thất bại");
//                    }
//                    one.setType("Xác minh");
//                }
//            }

            /*for (RpDetailInformation o: data1){
                System.out.println(o.toString());
            }*/
            model.addAttribute("data1", data1);
            model.addAttribute("data2", data2);
            model.addAttribute("not_found_message", not_found_message);
            model.addAttribute("allOrg", allOrg);
            model.addAttribute("orgId", orgId);

            //session
            session.setAttribute("fromDate", dateFormat.format(_fromDate));
            session.setAttribute("toDate", dateFormat.format(_toDate));
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "report/doisoat_chitiet";
    }

    @RequestMapping(value = "/doisoat_tonghop.html", method = {RequestMethod.GET, RequestMethod.POST})
    @PreAuthorize(value = "hasAnyAuthority('Administrators','Users')")
    public String tonghop(Model model,
                          HttpSession session,
                          Principal principal,
                          @RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                          @RequestParam(value = "toDate", defaultValue = "") String toDate,
                          @RequestParam(value = "orgId", defaultValue = "") String orgId) {
        try {
            //datetime
            DateTime dateTime = new DateTime();
            Date _fromDate = dateTime.withTimeAtStartOfDay().toDate();
            Date _toDate = dateTime.withTime(23, 59, 59, 999).toDate();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            try {
                _fromDate = dateFormat.parse(fromDate);
                _toDate = dateFormat.parse(toDate);
            } catch (Exception ex) {
            }
            model.addAttribute("fromDate", dateFormat.format(_fromDate));
            model.addAttribute("toDate", dateFormat.format(_toDate));

            Map<Integer, String> mapOrg = new HashMap<>();

            List<RpOrganization> allOrg;
            // lay org id tu username -- start
            Integer orgIdInt = getOrgIdFromUserName(principal.getName());
            RpOrganization orgCurrent = null;
            if (orgIdInt != null) {
                allOrg = new ArrayList<>();
                orgCurrent = rpOrganizationService.findById(orgIdInt);
                allOrg.add(orgCurrent);
            } else {
                allOrg = rpOrganizationService.findAll();
            }

            // lay org id tu username -- end
            for (RpOrganization one : allOrg) {
                mapOrg.put(one.getOrgId(), one.getOrgName());
            }

            List<ReportFullApiRequestModel> data = new ArrayList<>();
            List<Object[]> result;
            List<Object[]> result1;
            if (orgIdInt == null) {
                if (Strings.isNullOrEmpty(orgId)) {
                    result = apiRequestLogService.findReport(_fromDate, _toDate, null);
                    result1 = rpCampaignDailyResultService.findReportDailyResult(_fromDate, _toDate, null);
                } else {
                    RpOrganization org = rpOrganizationService.findById(Integer.valueOf(orgId));
                    if (orgCurrent != null) {
                        result = apiRequestLogService.findReport(_fromDate, _toDate, org.getOrgCode());
                    } else {
                        result = apiRequestLogService.findReport(_fromDate, _toDate, null);
                    }
                    result1 = rpCampaignDailyResultService.findReportDailyResult(_fromDate, _toDate, Integer.valueOf(orgId));
                }
            } else {
                if (orgCurrent != null) {
                    result = apiRequestLogService.findReport(_fromDate, _toDate, orgCurrent.getOrgCode());
                } else {
                    result = apiRequestLogService.findReport(_fromDate, _toDate, null);
                }
                result1 = rpCampaignDailyResultService.findReportDailyResult(_fromDate, _toDate, orgIdInt);
            }

            ReportFullApiRequestModel sum = new ReportFullApiRequestModel();
            sum.setTctd("Tổng");

            for (Object[] obj : result) {
                ReportFullApiRequestModel one = new ReportFullApiRequestModel();
                one.setTctd(obj[0].toString());
                one.setFreePoint(Double.valueOf(obj[1].toString()));
                one.setPaidPoint(Double.valueOf(obj[2].toString()));
                one.setAllPoint(Double.valueOf(obj[3].toString()));
                one.setFreeCus(Double.valueOf(obj[4].toString()));
                one.setPaidCus(Double.valueOf(obj[5].toString()));
                one.setAllCus(Double.valueOf(obj[6].toString()));
                one.setFreeFrau(Double.valueOf(obj[7].toString()));
                one.setPaidFrau(Double.valueOf(obj[8].toString()));
                one.setAllFrau(Double.valueOf(obj[9].toString()));

                sum.setFreePoint(sum.getFreePoint() + Double.valueOf(obj[1].toString()));
                sum.setPaidPoint(sum.getPaidPoint() + Double.valueOf(obj[2].toString()));
                sum.setAllPoint(sum.getAllPoint() + Double.valueOf(obj[3].toString()));
                sum.setFreeCus(sum.getFreeCus() + Double.valueOf(obj[4].toString()));
                sum.setPaidCus(sum.getPaidCus() + Double.valueOf(obj[5].toString()));
                sum.setAllCus(sum.getAllCus() + Double.valueOf(obj[6].toString()));
                sum.setFreeFrau(sum.getFreeFrau() + Double.valueOf(obj[7].toString()));
                sum.setPaidFrau(sum.getPaidFrau() + Double.valueOf(obj[8].toString()));
                sum.setAllFrau(sum.getAllFrau() + Double.valueOf(obj[9].toString()));
                data.add(one);
            }


            List<CampaignFullModel> data1 = new ArrayList<>();

            CampaignFullModel sum1 = new CampaignFullModel();
            for (Object[] obj : result1) {
                CampaignFullModel one = new CampaignFullModel();
                one.setReceiveDatetime((Date) obj[0]);
                one.setOrgName(mapOrg.get(Integer.valueOf(obj[1].toString())));
                one.setTotal(Double.valueOf(obj[2].toString()));

                sum1.setTotal(sum1.getTotal() + Double.valueOf(obj[2].toString()));
                data1.add(one);
            }

            model.addAttribute("data", data);
            model.addAttribute("data1", data1);
            model.addAttribute("sum", sum);
            model.addAttribute("sum1", sum1);
            model.addAttribute("allOrg", allOrg);
            model.addAttribute("orgId", orgId);
            //session
            session.setAttribute("fromDate", dateFormat.format(_fromDate));
            session.setAttribute("toDate", dateFormat.format(_toDate));
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "report/doisoat_tonghop";
    }

    /**
     * Lấy org id theo username
     *
     * @param userName
     * @return
     */
    private Integer getOrgIdFromUserName(String userName) {
        AuthUser authUser = authUserService.findByUsername(userName);
        if (authUser != null) {
            UserOrg userOrg = userOrgRepo.findByUserId(authUser.getId());
            if (userOrg != null) {
                return userOrg.getOrgId();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
