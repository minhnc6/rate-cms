package vn.yotel.ratecms;

public interface Constans {
    public String NOT_FOUND_MESSAGE = "Không tìm thấy dữ liệu";

    public interface Paging {
        public int SIZE = 50;
    }

    public interface Audit {
        public String CREATE = "CREATE";
        public String UPDATE = "UPDATE";
        public String DELETE = "DELETE";
        public String LOGIN = "LOGIN";
        public String LOGOUT = "LOGOUT";
    }

    public interface ServiceCode {
        public String SCORING = "scoring";
        public String CUSTOMER = "customer";

    }

    public static final Integer ACTIVE = 1;

    public static final Integer INACTIVE = 0;

    public static final String ACTIVE_TXT = "1";

    public static final String INACTIVE_TXT = "0";




}
