package vn.yotel.ratecms.service;

import vn.yotel.admin.jpa.Role;

import java.util.List;

public interface RoleService {
    List<Role> findByStatus(Integer status);
}
