package vn.yotel.ratecms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.yotel.ratecms.jpa.RpOrgServiceRegister;
import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.jpa.RpService;

import java.util.List;

public interface RpOrgServiceRegisterService {
    void create(RpOrgServiceRegister rp);
    RpOrgServiceRegister update(RpOrgServiceRegister rp);
    RpOrgServiceRegister findById(Integer id);
    void delete(RpOrgServiceRegister rp);
    List<RpOrgServiceRegister> findByRegisterStatus(String status);
    List<RpOrgServiceRegister> findByOrgIdAndRegisterStatus(RpOrganization orgId, String status);
    Page<RpOrgServiceRegister> findList(RpOrganization orgId, RpService serviceId, String status, Pageable page);
}
