package vn.yotel.ratecms.service;

import vn.yotel.ratecms.jpa.ApiRequestLog;

import java.util.Date;
import java.util.List;

public interface ApiRequestLogService {
    List<ApiRequestLog> findByRequestDatetime(Date fromDate, Date toDate, String orgCode);
    List<Object[]> findReport(Date fromDate, Date toDate, String orgCode);
    List<Object[]> findByRequestDatetimeNew(Date fromDate, Date toDate, String orgCode);
}
