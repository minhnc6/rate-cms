package vn.yotel.ratecms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.yotel.ratecms.jpa.Campaign;
import vn.yotel.ratecms.jpa.CampaignDailyResult;

import java.util.Date;
import java.util.List;

public interface RpCampaignDailyResultService {
    Page<CampaignDailyResult> findList(Date fromDate, Date toDate, String msisdn, Integer orgId,
                                       Integer productId, Integer msgId, Integer campaignId,Pageable page);

    List<Campaign> findCampaigns(String status);

    List<Campaign> findAllCamp();

    List<CampaignDailyResult> findList(Date fromDate, Date toDate, String msisdn, Integer orgId,
                                       Integer productId, Integer msgId, Integer campaignId);

    List<Object[]> findReportDailyResult(Date fromDate, Date toDate, Integer orgId);
}
