//package vn.yotel.ratecms.service;
//
//import vn.yotel.ratecms.jpa.RpResource;
//
//import java.util.List;
//
//public interface RpResourceService {
//    void create(RpResource rp);
//    RpResource update(RpResource rp);
//    void delete(RpResource rp);
//    RpResource findById(Long id);
//    RpResource findByName(String name);
//    List<RpResource> findByStatus(Integer status);
//    List<RpResource> findByNameAndStatus(String name, Integer status);
//}
