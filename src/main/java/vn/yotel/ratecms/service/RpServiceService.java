package vn.yotel.ratecms.service;

import vn.yotel.ratecms.jpa.RpService;

import java.util.List;

public interface RpServiceService {
    void create(RpService rp);
    RpService update(RpService rp);
    void delete(RpService rp);
    RpService findById(Integer id);
    RpService findByServiceName(String name);
    List<RpService> findByServiceStatus(String status);
    List<RpService> findList(String name, String status);
}
