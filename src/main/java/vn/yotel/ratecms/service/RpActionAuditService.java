package vn.yotel.ratecms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.yotel.ratecms.jpa.RpActionAudit;

import java.util.Date;

public interface RpActionAuditService {
    void create(RpActionAudit rp);
    Page<RpActionAudit> findList(Date fromDate, Date toDate, String userName, String userAction,
                                 String _object, Pageable page);
}
