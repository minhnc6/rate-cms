package vn.yotel.ratecms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.yotel.ratecms.jpa.RpOrganization;

import java.util.List;

public interface RpOrganizationService {
    void create(RpOrganization rp);
    RpOrganization update(RpOrganization rp);
    void delete(RpOrganization rp);
    RpOrganization findById(Integer id);
    RpOrganization findByOrgName(String name);
    RpOrganization findByOrgCode(String code);
    List<RpOrganization> findByOrgStatus(String status);
    List<RpOrganization> findList(String name, String status);
    Page<RpOrganization> findPage(String name, String code, String status, Pageable page);
    List<RpOrganization> findAll();
}
