package vn.yotel.ratecms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.jpa.RpService;
import vn.yotel.ratecms.jpa.RpServicePrice;

public interface RpServicePriceService {
    void create(RpServicePrice rp);
    void delete(RpServicePrice rp);
    RpServicePrice findById(Integer id);
    Page<RpServicePrice> findList(RpOrganization orgId, RpService serviceId, String status, Pageable page);
}
