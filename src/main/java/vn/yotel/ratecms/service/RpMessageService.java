package vn.yotel.ratecms.service;

import vn.yotel.ratecms.jpa.RpMessage;

import java.util.List;

public interface RpMessageService {
    void create(RpMessage rp);
    RpMessage update(RpMessage rp);
    void delete(RpMessage rp);
    RpMessage findById(Integer id);
    RpMessage findByMsgName(String name);
    List<RpMessage> findByMsgStatus(String status);
    List<RpMessage> findList(String name, String status);
}
