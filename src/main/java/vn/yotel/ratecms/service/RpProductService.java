package vn.yotel.ratecms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.yotel.ratecms.jpa.RpProduct;

import java.util.List;

public interface RpProductService {
    void create(RpProduct rp);
    RpProduct update(RpProduct rp);
    void delete(RpProduct rp);
    RpProduct findById(Integer id);
    RpProduct findByProductName(String name);
    List<RpProduct> findByProductStatus(String status);
    List<RpProduct> findByNameAndStatus(String name, String status);
    Page<RpProduct> findList(String name, String status, Pageable page);
    List<RpProduct> findByOrgIdAndProductStatus(Integer orgId, String status);
}
