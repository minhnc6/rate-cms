package vn.yotel.ratecms.service;



import vn.yotel.ratecms.jpa.RpResource;

import java.util.List;

public interface ResourceTblService {
    void create(RpResource rp);
    RpResource update(RpResource rp);
    void delete(RpResource rp);
    RpResource findById(Integer id);
    RpResource findByResourceName(String name);
    List<RpResource> findByResourceStatus(String status);
    List<RpResource> findList(String name, String status);
}
