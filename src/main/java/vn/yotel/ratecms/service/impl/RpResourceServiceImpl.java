//package vn.yotel.ratecms.service.impl;
//
//import com.google.common.base.Strings;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.RpService;
//import org.springframework.transaction.annotation.Transactional;
//import vn.yotel.ratecms.jpa.RpResource;
//import vn.yotel.ratecms.repository.RpResourceRepo;
//import vn.yotel.ratecms.service.RpResourceService;
//
//import java.util.List;
//
//@RpService
//@Transactional
//public class RpResourceServiceImpl implements RpResourceService {
//
//    @Autowired
//    RpResourceRepo rpResourceRepo;
//
//    @Override
//    public void create(RpResource rp) {
//        rpResourceRepo.save(rp);
//    }
//
//    @Override
//    public RpResource update(RpResource rp) {
//        return rpResourceRepo.save(rp);
//    }
//
//    @Override
//    public void delete(RpResource rp) {
//        rpResourceRepo.delete(rp);
//    }
//
//    @Override
//    public RpResource findById(Long id) {
//        return rpResourceRepo.findOne(id);
//    }
//
//    @Override
//    public RpResource findByName(String name) {
//        return rpResourceRepo.findByName(name);
//    }
//
//    @Override
//    public List<RpResource> findByStatus(Integer status) {
//        return rpResourceRepo.findByStatus(status);
//    }
//
//    @Override
//    public List<RpResource> findByNameAndStatus(String name, Integer status) {
//        if(!Strings.isNullOrEmpty(name)) {
//            name = "%" + name + "%";
//        }
//        return rpResourceRepo.findByNameAndStatus(name, status);
//    }
//}
