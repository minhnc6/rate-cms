package vn.yotel.ratecms.service.impl;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.RpProduct;
import vn.yotel.ratecms.repository.RpProductRepo;
import vn.yotel.ratecms.service.RpProductService;

import java.util.List;

@Service
@Transactional
public class RpProductServiceImpl implements RpProductService {

    @Autowired
    RpProductRepo rpProductRepo;

    @Override
    public void create(RpProduct rp) {
        rpProductRepo.save(rp);
    }

    @Override
    public RpProduct update(RpProduct rp) {
        return rpProductRepo.save(rp);
    }

    @Override
    public void delete(RpProduct rp) {
        rpProductRepo.delete(rp);
    }

    @Override
    public RpProduct findById(Integer id) {
        return rpProductRepo.findOne(id);
    }

    @Override
    public RpProduct findByProductName(String name) {
        return rpProductRepo.findByProductName(name);
    }

    @Override
    public List<RpProduct> findByProductStatus(String status) {
        return rpProductRepo.findByProductStatus(status);
    }

    @Override
    public List<RpProduct> findByNameAndStatus(String name, String status) {
        if(!Strings.isNullOrEmpty(name)) {
            name = "%" + name + "%";
        }
        return rpProductRepo.findByNameAndStatus(name, status);
    }

    @Override
    public Page<RpProduct> findList(String name, String status, Pageable page) {
        if(!Strings.isNullOrEmpty(name)) {
            name = "%" + name + "%";
        }
        return rpProductRepo.findList(name, status, page);
    }

    @Override
    public List<RpProduct> findByOrgIdAndProductStatus(Integer orgId, String status) {
        return rpProductRepo.findByOrgIdAndProductStatus(orgId, status);
    }
}
