package vn.yotel.ratecms.service.impl;

import org.springframework.stereotype.Service;
import vn.yotel.admin.jpa.Role;
//import vn.yotel.yologw.repository.RoleRepo;
import vn.yotel.admin.repository.AuthRoleRepo;
import vn.yotel.ratecms.repository.RoleRepo;
import vn.yotel.ratecms.service.RoleService;

import javax.annotation.Resource;
import java.util.List;

@Service(value = "roleService")
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleRepo roleRepo;

    @Override
    public List<Role> findByStatus(Integer status) {
        return roleRepo.findByStatus(status);
    }

}
