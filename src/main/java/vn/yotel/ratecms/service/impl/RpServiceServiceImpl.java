package vn.yotel.ratecms.service.impl;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.RpService;
import vn.yotel.ratecms.repository.RpServiceRepo;
import vn.yotel.ratecms.service.RpServiceService;

import java.util.List;

@Service
@Transactional
public class RpServiceServiceImpl implements RpServiceService {

    @Autowired
    RpServiceRepo rpServiceRepo;

    @Override
    public void create(RpService rp) {
        rpServiceRepo.save(rp);
    }

    @Override
    public RpService update(RpService rp) {
        return rpServiceRepo.save(rp);
    }

    @Override
    public void delete(RpService rp) {
        rpServiceRepo.delete(rp);
    }

    @Override
    public RpService findById(Integer id) {
        return rpServiceRepo.findOne(id);
    }

    @Override
    public RpService findByServiceName(String name) {
        return rpServiceRepo.findByServiceName(name);
    }

    @Override
    public List<RpService> findByServiceStatus(String status) {
        return rpServiceRepo.findByServiceStatus(status);
    }

    @Override
    public List<RpService> findList(String name, String status) {
        if(!Strings.isNullOrEmpty(name)) {
            name = "%" + name + "%";
        }
        return rpServiceRepo.findList(name, status);
    }
}
