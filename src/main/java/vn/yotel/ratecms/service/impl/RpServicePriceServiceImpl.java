package vn.yotel.ratecms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.jpa.RpService;
import vn.yotel.ratecms.jpa.RpServicePrice;
import vn.yotel.ratecms.repository.RpServicePriceRepo;
import vn.yotel.ratecms.service.RpServicePriceService;

@Service
@Transactional
public class RpServicePriceServiceImpl implements RpServicePriceService {

    @Autowired
    RpServicePriceRepo rpServicePriceRepo;

    @Override
    public void create(RpServicePrice rp) {
        rpServicePriceRepo.save(rp);
    }

    @Override
    public void delete(RpServicePrice rp) {
        rpServicePriceRepo.delete(rp);
    }

    @Override
    public RpServicePrice findById(Integer id) {
        return rpServicePriceRepo.findOne(id);
    }

    @Override
    public Page<RpServicePrice> findList(RpOrganization orgId, RpService serviceId, String status, Pageable page) {
        return rpServicePriceRepo.findList(orgId, serviceId, status, page);
    }
}
