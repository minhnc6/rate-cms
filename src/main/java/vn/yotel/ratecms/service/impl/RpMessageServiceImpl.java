package vn.yotel.ratecms.service.impl;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.RpMessage;
import vn.yotel.ratecms.repository.RpMessageRepo;
import vn.yotel.ratecms.service.RpMessageService;

import java.util.List;

@Service
@Transactional
public class RpMessageServiceImpl implements RpMessageService {

    @Autowired
    RpMessageRepo rpMessageRepo;

    @Override
    public void create(RpMessage rp) {
        rpMessageRepo.save(rp);
    }

    @Override
    public RpMessage update(RpMessage rp) {
        return rpMessageRepo.save(rp);
    }

    @Override
    public void delete(RpMessage rp) {
        rpMessageRepo.delete(rp);
    }

    @Override
    public RpMessage findById(Integer id) {
        return rpMessageRepo.findOne(id);
    }

    @Override
    public RpMessage findByMsgName(String name) {
        return rpMessageRepo.findByMsgName(name);
    }

    @Override
    public List<RpMessage> findByMsgStatus(String status) {
        return rpMessageRepo.findByMsgStatus(status);
    }

    @Override
    public List<RpMessage> findList(String name, String status) {
        if(!Strings.isNullOrEmpty(name)) {
            name = "%" + name + "%";
        }
        return rpMessageRepo.findList(name, status);
    }
}
