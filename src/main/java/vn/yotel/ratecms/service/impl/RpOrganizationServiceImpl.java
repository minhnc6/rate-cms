package vn.yotel.ratecms.service.impl;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.repository.RpOrganizationRepo;
import vn.yotel.ratecms.service.RpOrganizationService;

import java.util.List;

@Service
@Transactional
public class RpOrganizationServiceImpl implements RpOrganizationService {

    @Autowired
    RpOrganizationRepo rpOrganizationRepo;

    @Override
    public void create(RpOrganization rp) {
        rpOrganizationRepo.save(rp);
    }

    @Override
    public RpOrganization update(RpOrganization rp) {
        return rpOrganizationRepo.save(rp);
    }

    @Override
    public void delete(RpOrganization rp) {
        rpOrganizationRepo.delete(rp);
    }

    @Override
    public RpOrganization findById(Integer id) {
        return rpOrganizationRepo.findOne(id);
    }

    @Override
    public RpOrganization findByOrgName(String name) {
        return rpOrganizationRepo.findByOrgName(name);
    }

    @Override
    public RpOrganization findByOrgCode(String code) {
        return rpOrganizationRepo.findByOrgCode(code);
    }

    @Override
    public List<RpOrganization> findByOrgStatus(String status) {
        return rpOrganizationRepo.findByOrgStatus(status);
    }

    @Override
    public List<RpOrganization> findList(String name, String status) {
        if(!Strings.isNullOrEmpty(name)) {
            name = "%" + name + "%";
        }
        return rpOrganizationRepo.findList(name, status);
    }

    @Override
    public Page<RpOrganization> findPage(String name, String code, String status, Pageable page) {
        if(!Strings.isNullOrEmpty(name)) {
            name = "%" + name + "%";
        }
        return rpOrganizationRepo.findPage(name, code, status, page);
    }

    @Override
    public List<RpOrganization> findAll() {
        return rpOrganizationRepo.findAll();
    }
}
