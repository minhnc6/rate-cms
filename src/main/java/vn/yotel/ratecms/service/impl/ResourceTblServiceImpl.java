package vn.yotel.ratecms.service.impl;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.RpResource;
import vn.yotel.ratecms.repository.ResourceTblRepo;
import vn.yotel.ratecms.service.ResourceTblService;

import java.util.List;

@Service
@Transactional
public class ResourceTblServiceImpl implements ResourceTblService {

    @Autowired
    ResourceTblRepo rpResourceTblRepo;

    @Override
    public void create(RpResource rp) {
        rpResourceTblRepo.save(rp);
    }

    @Override
    public RpResource update(RpResource rp) {
        return rpResourceTblRepo.save(rp);
    }

    @Override
    public void delete(RpResource rp) {
        rpResourceTblRepo.delete(rp);
    }

    @Override
    public RpResource findById(Integer id) {
        return rpResourceTblRepo.findOne(id);
    }

    @Override
    public RpResource findByResourceName(String name) {
        return rpResourceTblRepo.findByResourceName(name);
    }

    @Override
    public List<RpResource> findByResourceStatus(String status) {
        return rpResourceTblRepo.findByResourceStatus(status);
    }

    @Override
    public List<RpResource> findList(String name, String status) {
        if(!Strings.isNullOrEmpty(name)) {
            name = "%" + name + "%";
        }
        return rpResourceTblRepo.findList(name, status);
    }
}
