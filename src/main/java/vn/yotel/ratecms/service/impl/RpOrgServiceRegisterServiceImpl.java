package vn.yotel.ratecms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.RpOrgServiceRegister;
import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.jpa.RpService;
import vn.yotel.ratecms.repository.RpOrgServiceRegisterRepo;
import vn.yotel.ratecms.service.RpOrgServiceRegisterService;


import java.util.List;

@Service
@Transactional
public class RpOrgServiceRegisterServiceImpl implements RpOrgServiceRegisterService {

    @Autowired
    RpOrgServiceRegisterRepo rpOrgServiceRegisterRepo;

    @Override
    public void create(RpOrgServiceRegister rp) {
        rpOrgServiceRegisterRepo.save(rp);
    }

    @Override
    public RpOrgServiceRegister update(RpOrgServiceRegister rp) {
        return rpOrgServiceRegisterRepo.save(rp);
    }

    @Override
    public RpOrgServiceRegister findById(Integer id) {
        return rpOrgServiceRegisterRepo.findOne(id);
    }

    @Override
    public void delete(RpOrgServiceRegister rp) {
        rpOrgServiceRegisterRepo.delete(rp);
    }

    @Override
    public List<RpOrgServiceRegister> findByRegisterStatus(String status) {
        return rpOrgServiceRegisterRepo.findByRegisterStatus(status);
    }

    @Override
    public List<RpOrgServiceRegister> findByOrgIdAndRegisterStatus(RpOrganization orgId, String status) {
        return rpOrgServiceRegisterRepo.findByOrgIdAndRegisterStatus(orgId, status);
    }

    @Override
    public Page<RpOrgServiceRegister> findList(RpOrganization orgId, RpService serviceId, String status, Pageable page) {
        return rpOrgServiceRegisterRepo.findList(orgId, serviceId, status, page);
    }
}
