package vn.yotel.ratecms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.ApiRequestLog;
import vn.yotel.ratecms.repository.ApiRequestLogRepo;
import vn.yotel.ratecms.service.ApiRequestLogService;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ApiRequestLogServiceImpl implements ApiRequestLogService {

    @Autowired
    ApiRequestLogRepo apiRequestLogRepo;

    @Override
    public List<ApiRequestLog> findByRequestDatetime(Date fromDate, Date toDate, String orgCode) {
        return apiRequestLogRepo.findByRequestDatetime(fromDate, toDate, orgCode);
    }

    @Override
    public List<Object[]> findReport(Date fromDate, Date toDate, String orgCode) {
        return apiRequestLogRepo.findReport(fromDate, toDate, orgCode);
    }

    @Override
    public List<Object[]> findByRequestDatetimeNew(Date fromDate, Date toDate, String orgCode) {
        return apiRequestLogRepo.findByRequestDatetimeNew(fromDate, toDate, orgCode);
    }
}
