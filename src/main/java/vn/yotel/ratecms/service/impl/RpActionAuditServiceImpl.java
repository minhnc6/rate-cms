package vn.yotel.ratecms.service.impl;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.RpActionAudit;
import vn.yotel.ratecms.repository.RpActionAuditRepo;
import vn.yotel.ratecms.service.RpActionAuditService;

import java.util.Date;

@Service
@Transactional
public class RpActionAuditServiceImpl implements RpActionAuditService {

    @Autowired
    RpActionAuditRepo rpActionAuditRepo;

    @Override
    public void create(RpActionAudit rp) {
        rpActionAuditRepo.save(rp);
    }

    @Override
    public Page<RpActionAudit> findList(Date fromDate, Date toDate, String userName, String userAction, String _object, Pageable page) {
        if(!Strings.isNullOrEmpty(userName)) {
            userName = "%" + userName + "%";
        }
        if(!Strings.isNullOrEmpty(userAction)) {
            userAction = "%" + userAction + "%";
        }
        if(!Strings.isNullOrEmpty(_object)) {
            _object = "%" + _object + "%";
        }
        return rpActionAuditRepo.findList(fromDate, toDate, userName, userAction, _object, page);
    }
}
