package vn.yotel.ratecms.service.impl;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.yotel.ratecms.jpa.Campaign;
import vn.yotel.ratecms.jpa.CampaignDailyResult;
import vn.yotel.ratecms.repository.CampaignRepo;
import vn.yotel.ratecms.repository.RpCampaignDailyResultRepo;
import vn.yotel.ratecms.service.RpCampaignDailyResultService;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class RpCampaignDailyResultServiceImpl implements RpCampaignDailyResultService {

    @Autowired
    RpCampaignDailyResultRepo rpCampaignDailyResultRepo;

    @Autowired
    CampaignRepo campaignRepo;

    @Override
    public Page<CampaignDailyResult> findList(Date fromDate, Date toDate, String msisdn, Integer orgId, Integer productId, Integer msgId, Integer campaignId, Pageable page) {
        return rpCampaignDailyResultRepo.findList(fromDate, toDate, msisdn, orgId, productId, msgId, campaignId, page);
    }

    @Override
    public List<Campaign> findCampaigns(String status) {
        return campaignRepo.findAll();
    }

    @Override
    public List<CampaignDailyResult> findList(Date fromDate, Date toDate, String msisdn, Integer orgId, Integer productId, Integer msgId, Integer campaignId) {
        return rpCampaignDailyResultRepo.findList(fromDate, toDate, msisdn, orgId, productId, msgId, campaignId);
    }

    @Override
    public List<Object[]> findReportDailyResult(Date fromDate, Date toDate, Integer orgId) {
        return rpCampaignDailyResultRepo.findReportDailyResult(fromDate, toDate, orgId);
    }

    @Override
    public List<Campaign> findAllCamp() {
        return campaignRepo.findAll();
    }
}
