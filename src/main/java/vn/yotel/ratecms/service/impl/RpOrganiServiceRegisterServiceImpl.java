//package vn.yotel.ratecms.service.impl;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.stereotype.RpService;
//import org.springframework.transaction.annotation.Transactional;
//import vn.yotel.ratecms.jpa.RpOrganiServiceRegister;
//import vn.yotel.ratecms.repository.RpOrganiServiceRegisterRepo;
//import vn.yotel.ratecms.service.RpOrganiServiceRegisterService;
//
//import java.util.List;
//
//@RpService
//@Transactional
//public class RpOrganiServiceRegisterServiceImpl implements RpOrganiServiceRegisterService {
//
//    @Autowired
//    RpOrganiServiceRegisterRepo rpOrganiServiceRegisterRepo;
//
//    @Override
//    public void create(RpOrganiServiceRegister rp) {
//        rpOrganiServiceRegisterRepo.save(rp);
//    }
//
//    @Override
//    public RpOrganiServiceRegister update(RpOrganiServiceRegister rp) {
//        return rpOrganiServiceRegisterRepo.save(rp);
//    }
//
//    @Override
//    public RpOrganiServiceRegister findById(Long id) {
//        return rpOrganiServiceRegisterRepo.findOne(id);
//    }
//
//    @Override
//    public void delete(RpOrganiServiceRegister rp) {
//        rpOrganiServiceRegisterRepo.delete(rp);
//    }
//
//    @Override
//    public List<RpOrganiServiceRegister> findByStatus(Integer status) {
//        return rpOrganiServiceRegisterRepo.findByStatus(status);
//    }
//
//    @Override
//    public List<RpOrganiServiceRegister> findByOrgIdAndStatus(Long orgId, Integer status) {
//        return rpOrganiServiceRegisterRepo.findByOrgIdAndStatus(orgId, status);
//    }
//
//    @Override
//    public Page<RpOrganiServiceRegister> findList(Long orgId, Long serviceId, Integer status, Pageable page) {
//        return rpOrganiServiceRegisterRepo.findList(orgId, serviceId, status, page);
//    }
//}
