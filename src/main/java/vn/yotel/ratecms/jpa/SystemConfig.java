/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "system_config")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "SystemConfig.findAll", query = "SELECT s FROM SystemConfig s")
//    , @NamedQuery(name = "SystemConfig.findByCfgId", query = "SELECT s FROM SystemConfig s WHERE s.cfgId = :cfgId")
//    , @NamedQuery(name = "SystemConfig.findByCfgName", query = "SELECT s FROM SystemConfig s WHERE s.cfgName = :cfgName")
//    , @NamedQuery(name = "SystemConfig.findByCfgType", query = "SELECT s FROM SystemConfig s WHERE s.cfgType = :cfgType")
//    , @NamedQuery(name = "SystemConfig.findByCfgValue", query = "SELECT s FROM SystemConfig s WHERE s.cfgValue = :cfgValue")
//    , @NamedQuery(name = "SystemConfig.findByCfgDescription", query = "SELECT s FROM SystemConfig s WHERE s.cfgDescription = :cfgDescription")
//    , @NamedQuery(name = "SystemConfig.findByCfgStatus", query = "SELECT s FROM SystemConfig s WHERE s.cfgStatus = :cfgStatus")})
public class SystemConfig implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cfg_id")
    private Integer cfgId;
    @Column(name = "cfg_name")
    private String cfgName;
    @Column(name = "cfg_type")
    private String cfgType;
    @Column(name = "cfg_value")
    private String cfgValue;
    @Column(name = "cfg_description")
    private String cfgDescription;
    @Column(name = "cfg_status")
    private String cfgStatus;

    public SystemConfig() {
    }

    public SystemConfig(Integer cfgId) {
        this.cfgId = cfgId;
    }

    public Integer getCfgId() {
        return cfgId;
    }

    public void setCfgId(Integer cfgId) {
        this.cfgId = cfgId;
    }

    public String getCfgName() {
        return cfgName;
    }

    public void setCfgName(String cfgName) {
        this.cfgName = cfgName;
    }

    public String getCfgType() {
        return cfgType;
    }

    public void setCfgType(String cfgType) {
        this.cfgType = cfgType;
    }

    public String getCfgValue() {
        return cfgValue;
    }

    public void setCfgValue(String cfgValue) {
        this.cfgValue = cfgValue;
    }

    public String getCfgDescription() {
        return cfgDescription;
    }

    public void setCfgDescription(String cfgDescription) {
        this.cfgDescription = cfgDescription;
    }

    public String getCfgStatus() {
        return cfgStatus;
    }

    public void setCfgStatus(String cfgStatus) {
        this.cfgStatus = cfgStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cfgId != null ? cfgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystemConfig)) {
            return false;
        }
        SystemConfig other = (SystemConfig) object;
        if ((this.cfgId == null && other.cfgId != null) || (this.cfgId != null && !this.cfgId.equals(other.cfgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.SystemConfig[ cfgId=" + cfgId + " ]";
    }

}
