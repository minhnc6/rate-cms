/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Admin
 */
@Embeddable
public class RpActionAuditPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "audit_id")
    private int auditId;
    @Basic(optional = false)
    @Column(name = "action_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actionDatetime;

    public RpActionAuditPK() {
    }

    public RpActionAuditPK(int auditId, Date actionDatetime) {
        this.auditId = auditId;
        this.actionDatetime = actionDatetime;
    }

    public int getAuditId() {
        return auditId;
    }

    public void setAuditId(int auditId) {
        this.auditId = auditId;
    }

    public Date getActionDatetime() {
        return actionDatetime;
    }

    public void setActionDatetime(Date actionDatetime) {
        this.actionDatetime = actionDatetime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) auditId;
        hash += (actionDatetime != null ? actionDatetime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpActionAuditPK)) {
            return false;
        }
        RpActionAuditPK other = (RpActionAuditPK) object;
        if (this.auditId != other.auditId) {
            return false;
        }
        if ((this.actionDatetime == null && other.actionDatetime != null) || (this.actionDatetime != null && !this.actionDatetime.equals(other.actionDatetime))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.RpActionAuditPK[ auditId=" + auditId + ", actionDatetime=" + actionDatetime + " ]";
    }

    @PrePersist
    public void prePersist(){
        if(this.actionDatetime == null){
            this.actionDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }
}
