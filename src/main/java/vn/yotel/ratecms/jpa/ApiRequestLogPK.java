/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Embeddable
public class ApiRequestLogPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "request_log_id")
    private int requestLogId;
    @Basic(optional = false)
    @Column(name = "request_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDatetime;

    public ApiRequestLogPK() {
    }

    public ApiRequestLogPK(int requestLogId, Date requestDatetime) {
        this.requestLogId = requestLogId;
        this.requestDatetime = requestDatetime;
    }

    public ApiRequestLogPK(Date requestDatetime) {
        this.requestDatetime = requestDatetime;
    }

    public int getRequestLogId() {
        return requestLogId;
    }

    public void setRequestLogId(int requestLogId) {
        this.requestLogId = requestLogId;
    }

    public Date getRequestDatetime() {
        return requestDatetime;
    }

    public void setRequestDatetime(Date requestDatetime) {
        this.requestDatetime = requestDatetime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) requestLogId;
        hash += (requestDatetime != null ? requestDatetime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApiRequestLogPK)) {
            return false;
        }
        ApiRequestLogPK other = (ApiRequestLogPK) object;
        if (this.requestLogId != other.requestLogId) {
            return false;
        }
        if ((this.requestDatetime == null && other.requestDatetime != null) || (this.requestDatetime != null && !this.requestDatetime.equals(other.requestDatetime))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.ApiRequestLogPK[ requestLogId=" + requestLogId + ", requestDatetime=" + requestDatetime + " ]";
    }

}
