/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "campaign_daily_result")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "CampaignDailyResult.findAll", query = "SELECT c FROM CampaignDailyResult c")
//    , @NamedQuery(name = "CampaignDailyResult.findByResultId", query = "SELECT c FROM CampaignDailyResult c WHERE c.campaignDailyResultPK.resultId = :resultId")
//    , @NamedQuery(name = "CampaignDailyResult.findByOrgId", query = "SELECT c FROM CampaignDailyResult c WHERE c.orgId = :orgId")
//    , @NamedQuery(name = "CampaignDailyResult.findByProductId", query = "SELECT c FROM CampaignDailyResult c WHERE c.productId = :productId")
//    , @NamedQuery(name = "CampaignDailyResult.findByMsgId", query = "SELECT c FROM CampaignDailyResult c WHERE c.msgId = :msgId")
//    , @NamedQuery(name = "CampaignDailyResult.findByCampaignId", query = "SELECT c FROM CampaignDailyResult c WHERE c.campaignId = :campaignId")
//    , @NamedQuery(name = "CampaignDailyResult.findByIsdn", query = "SELECT c FROM CampaignDailyResult c WHERE c.isdn = :isdn")
//    , @NamedQuery(name = "CampaignDailyResult.findByMo", query = "SELECT c FROM CampaignDailyResult c WHERE c.mo = :mo")
//    , @NamedQuery(name = "CampaignDailyResult.findByMoDatetime", query = "SELECT c FROM CampaignDailyResult c WHERE c.moDatetime = :moDatetime")
//    , @NamedQuery(name = "CampaignDailyResult.findByReceiveDatetime", query = "SELECT c FROM CampaignDailyResult c WHERE c.campaignDailyResultPK.receiveDatetime = :receiveDatetime")
//    , @NamedQuery(name = "CampaignDailyResult.findByInsertDatetime", query = "SELECT c FROM CampaignDailyResult c WHERE c.insertDatetime = :insertDatetime")})
public class CampaignDailyResult implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CampaignDailyResultPK campaignDailyResultPK;
    @Column(name = "org_id")
    private Integer orgId;
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "msg_id")
    private Integer msgId;
    @Column(name = "campaign_id")
    private Integer campaignId;
    @Column(name = "isdn")
    private String isdn;
    @Column(name = "mo")
    private String mo;
    @Column(name = "mo_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date moDatetime;
    @Column(name = "insert_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDatetime;

    public CampaignDailyResult() {
    }

    public CampaignDailyResult(CampaignDailyResultPK campaignDailyResultPK) {
        this.campaignDailyResultPK = campaignDailyResultPK;
    }

    public CampaignDailyResult(int resultId, Date receiveDatetime) {
        this.campaignDailyResultPK = new CampaignDailyResultPK(resultId, receiveDatetime);
    }

    public CampaignDailyResultPK getCampaignDailyResultPK() {
        return campaignDailyResultPK;
    }

    public void setCampaignDailyResultPK(CampaignDailyResultPK campaignDailyResultPK) {
        this.campaignDailyResultPK = campaignDailyResultPK;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getMsgId() {
        return msgId;
    }

    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getMo() {
        return mo;
    }

    public void setMo(String mo) {
        this.mo = mo;
    }

    public Date getMoDatetime() {
        return moDatetime;
    }

    public void setMoDatetime(Date moDatetime) {
        this.moDatetime = moDatetime;
    }

    public Date getInsertDatetime() {
        return insertDatetime;
    }

    public void setInsertDatetime(Date insertDatetime) {
        this.insertDatetime = insertDatetime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (campaignDailyResultPK != null ? campaignDailyResultPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampaignDailyResult)) {
            return false;
        }
        CampaignDailyResult other = (CampaignDailyResult) object;
        if ((this.campaignDailyResultPK == null && other.campaignDailyResultPK != null) || (this.campaignDailyResultPK != null && !this.campaignDailyResultPK.equals(other.campaignDailyResultPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.CampaignDailyResult[ campaignDailyResultPK=" + campaignDailyResultPK + " ]";
    }

}
