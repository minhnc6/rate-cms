package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "service_price")
@XmlRootElement
//@NamedQueries({
//        @NamedQuery(name = "RpServicePrice.findAll", query = "SELECT s FROM RpServicePrice s")
//        , @NamedQuery(name = "RpServicePrice.findByPriceId", query = "SELECT s FROM RpServicePrice s WHERE s.priceId = :priceId")
//        , @NamedQuery(name = "RpServicePrice.findByStartDatetime", query = "SELECT s FROM RpServicePrice s WHERE s.startDatetime = :startDatetime")
//        , @NamedQuery(name = "RpServicePrice.findByEndDatetime", query = "SELECT s FROM RpServicePrice s WHERE s.endDatetime = :endDatetime")
//        , @NamedQuery(name = "RpServicePrice.findByPrice", query = "SELECT s FROM RpServicePrice s WHERE s.price = :price")
//        , @NamedQuery(name = "RpServicePrice.findByRegex", query = "SELECT s FROM RpServicePrice s WHERE s.regex = :regex")
//        , @NamedQuery(name = "RpServicePrice.findByPriceStatus", query = "SELECT s FROM RpServicePrice s WHERE s.priceStatus = :priceStatus")
//        , @NamedQuery(name = "RpServicePrice.findByCreateDatetime", query = "SELECT s FROM RpServicePrice s WHERE s.createDatetime = :createDatetime")
//        , @NamedQuery(name = "RpServicePrice.findByCreateUser", query = "SELECT s FROM RpServicePrice s WHERE s.createUser = :createUser")
//        , @NamedQuery(name = "RpServicePrice.findByUpdateDatetime", query = "SELECT s FROM RpServicePrice s WHERE s.updateDatetime = :updateDatetime")
//        , @NamedQuery(name = "RpServicePrice.findByUpdateUser", query = "SELECT s FROM RpServicePrice s WHERE s.updateUser = :updateUser")})
public class RpServicePrice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "price_id")
    private Integer priceId;
    @Column(name = "start_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDatetime;
    @Column(name = "end_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDatetime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Column(name = "regex")
    private String regex;
    @Column(name = "price_status")
    private String priceStatus;
    @Column(name = "create_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDatetime;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "update_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDatetime;
    @Column(name = "update_user")
    private String updateUser;
    @JoinColumn(name = "service_id", referencedColumnName = "service_id")
    @ManyToOne
    private RpService serviceId;
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")
    @ManyToOne
    private RpOrganization orgId;

    public RpServicePrice() {
    }

    public RpServicePrice(Integer priceId) {
        this.priceId = priceId;
    }

    public Integer getPriceId() {
        return priceId;
    }

    public void setPriceId(Integer priceId) {
        this.priceId = priceId;
    }

    public Date getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getPriceStatus() {
        return priceStatus;
    }

    public void setPriceStatus(String priceStatus) {
        this.priceStatus = priceStatus;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public RpService getServiceId() {
        return serviceId;
    }

    public void setServiceId(RpService serviceId) {
        this.serviceId = serviceId;
    }

    public RpOrganization getOrgId() {
        return orgId;
    }

    public void setOrgId(RpOrganization orgId) {
        this.orgId = orgId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (priceId != null ? priceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpServicePrice)) {
            return false;
        }
        RpServicePrice other = (RpServicePrice) object;
        if ((this.priceId == null && other.priceId != null) || (this.priceId != null && !this.priceId.equals(other.priceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.RpServicePrice[ priceId=" + priceId + " ]";
    }

    @PrePersist
    public void prePersist(){
        if(this.createDatetime == null){
            this.createDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

    @PreUpdate
    public void preUpdate(){
        if(this.updateDatetime == null){
            this.updateDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }
}
