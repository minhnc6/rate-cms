/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "message")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "RpMessage.findAll", query = "SELECT m FROM RpMessage m")
//    , @NamedQuery(name = "RpMessage.findByMsgId", query = "SELECT m FROM RpMessage m WHERE m.msgId = :msgId")
//    , @NamedQuery(name = "RpMessage.findByMsgName", query = "SELECT m FROM RpMessage m WHERE m.msgName = :msgName")
//    , @NamedQuery(name = "RpMessage.findByContent", query = "SELECT m FROM RpMessage m WHERE m.content = :content")
//    , @NamedQuery(name = "RpMessage.findByMsgDescription", query = "SELECT m FROM RpMessage m WHERE m.msgDescription = :msgDescription")
//    , @NamedQuery(name = "RpMessage.findByMsgStatus", query = "SELECT m FROM RpMessage m WHERE m.msgStatus = :msgStatus")
//    , @NamedQuery(name = "RpMessage.findByCreateDatetime", query = "SELECT m FROM RpMessage m WHERE m.createDatetime = :createDatetime")
//    , @NamedQuery(name = "RpMessage.findByCreateUser", query = "SELECT m FROM RpMessage m WHERE m.createUser = :createUser")
//    , @NamedQuery(name = "RpMessage.findByUpdateDatetime", query = "SELECT m FROM RpMessage m WHERE m.updateDatetime = :updateDatetime")
//    , @NamedQuery(name = "RpMessage.findByUpdateUser", query = "SELECT m FROM RpMessage m WHERE m.updateUser = :updateUser")})
public class RpMessage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "msg_id")
    private Integer msgId;
    @Column(name = "msg_name")
    private String msgName;
    @Column(name = "content")
    private String content;
    @Column(name = "msg_description")
    private String msgDescription;
    @Column(name = "msg_status")
    private String msgStatus;
    @Column(name = "create_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDatetime;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "update_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDatetime;
    @Column(name = "update_user")
    private String updateUser;

    public RpMessage() {
    }

    public RpMessage(Integer msgId) {
        this.msgId = msgId;
    }

    public Integer getMsgId() {
        return msgId;
    }

    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }

    public String getMsgName() {
        return msgName;
    }

    public void setMsgName(String msgName) {
        this.msgName = msgName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMsgDescription() {
        return msgDescription;
    }

    public void setMsgDescription(String msgDescription) {
        this.msgDescription = msgDescription;
    }

    public String getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(String msgStatus) {
        this.msgStatus = msgStatus;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (msgId != null ? msgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpMessage)) {
            return false;
        }
        RpMessage other = (RpMessage) object;
        if ((this.msgId == null && other.msgId != null) || (this.msgId != null && !this.msgId.equals(other.msgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RpMessage{" +
                "msgId=" + msgId +
                ", msgName='" + msgName + '\'' +
                ", content='" + content + '\'' +
                ", msgDescription='" + msgDescription + '\'' +
                ", msgStatus='" + msgStatus + '\'' +
                ", createDatetime=" + createDatetime +
                ", createUser='" + createUser + '\'' +
                ", updateDatetime=" + updateDatetime +
                ", updateUser='" + updateUser + '\'' +
                '}';
    }

    @PrePersist
    public void prePersist(){
        if(this.createDatetime == null){
            this.createDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

    @PreUpdate
    public void preUpdate(){
        if(this.updateDatetime == null){
            this.updateDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }
}
