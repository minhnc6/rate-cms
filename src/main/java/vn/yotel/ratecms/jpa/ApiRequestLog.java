/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "api_request_log")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "ApiRequestLog.findAll", query = "SELECT a FROM ApiRequestLog a")
//    , @NamedQuery(name = "ApiRequestLog.findByRequestLogId", query = "SELECT a FROM ApiRequestLog a WHERE a.apiRequestLogPK.requestLogId = :requestLogId")
//    , @NamedQuery(name = "ApiRequestLog.findByServiceCode", query = "SELECT a FROM ApiRequestLog a WHERE a.serviceCode = :serviceCode")
//    , @NamedQuery(name = "ApiRequestLog.findByOrganizationCode", query = "SELECT a FROM ApiRequestLog a WHERE a.organizationCode = :organizationCode")
//    , @NamedQuery(name = "ApiRequestLog.findByIpAddress", query = "SELECT a FROM ApiRequestLog a WHERE a.ipAddress = :ipAddress")
//    , @NamedQuery(name = "ApiRequestLog.findByDirection", query = "SELECT a FROM ApiRequestLog a WHERE a.direction = :direction")
//    , @NamedQuery(name = "ApiRequestLog.findByRequestDatetime", query = "SELECT a FROM ApiRequestLog a WHERE a.apiRequestLogPK.requestDatetime = :requestDatetime")
//    , @NamedQuery(name = "ApiRequestLog.findByIsdn", query = "SELECT a FROM ApiRequestLog a WHERE a.isdn = :isdn")
//    , @NamedQuery(name = "ApiRequestLog.findByRequest", query = "SELECT a FROM ApiRequestLog a WHERE a.request = :request")
//    , @NamedQuery(name = "ApiRequestLog.findByResponse", query = "SELECT a FROM ApiRequestLog a WHERE a.response = :response")
//    , @NamedQuery(name = "ApiRequestLog.findByTransId", query = "SELECT a FROM ApiRequestLog a WHERE a.transId = :transId")
//    , @NamedQuery(name = "ApiRequestLog.findByCharge", query = "SELECT a FROM ApiRequestLog a WHERE a.charge = :charge")
//    , @NamedQuery(name = "ApiRequestLog.findByErrorCode", query = "SELECT a FROM ApiRequestLog a WHERE a.errorCode = :errorCode")
//    , @NamedQuery(name = "ApiRequestLog.findByStatus", query = "SELECT a FROM ApiRequestLog a WHERE a.status = :status")})
public class ApiRequestLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ApiRequestLogPK apiRequestLogPK;
    @Column(name = "service_code")
    private String serviceCode;
    @Column(name = "organization_code")
    private String organizationCode;
    @Column(name = "ip_address")
    private String ipAddress;
    @Column(name = "direction")
    private String direction;
    @Column(name = "isdn")
    private String isdn;
    @Column(name = "request")
    private String request;
    @Column(name = "response")
    private String response;
    @Column(name = "trans_id")
    private String transId;
    @Column(name = "charge")
    private byte charge;
    @Column(name = "error_code")
    private String errorCode;
    @Column(name = "status")
    private String status;

    @Transient
    private String result;

    @Transient
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ApiRequestLog() {
    }

    public ApiRequestLog(ApiRequestLogPK apiRequestLogPK) {
        this.apiRequestLogPK = apiRequestLogPK;
    }

    public ApiRequestLog(int requestLogId, Date requestDatetime) {
        this.apiRequestLogPK = new ApiRequestLogPK(requestLogId, requestDatetime);
    }

    public ApiRequestLogPK getApiRequestLogPK() {
        return apiRequestLogPK;
    }

    public void setApiRequestLogPK(ApiRequestLogPK apiRequestLogPK) {
        this.apiRequestLogPK = apiRequestLogPK;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public byte getCharge() {
        return charge;
    }

    public void setCharge(Byte charge) {
        this.charge = charge;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (apiRequestLogPK != null ? apiRequestLogPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApiRequestLog)) {
            return false;
        }
        ApiRequestLog other = (ApiRequestLog) object;
        if ((this.apiRequestLogPK == null && other.apiRequestLogPK != null) || (this.apiRequestLogPK != null && !this.apiRequestLogPK.equals(other.apiRequestLogPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.ApiRequestLog[ apiRequestLogPK=" + apiRequestLogPK + " ]";
    }

}
