package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "service")
@XmlRootElement
//@NamedQueries({
//        @NamedQuery(name = "Service.findAll", query = "SELECT s FROM RpService s")
//        , @NamedQuery(name = "Service.findByServiceId", query = "SELECT s FROM RpService s WHERE s.serviceId = :serviceId")
//        , @NamedQuery(name = "Service.findByServiceName", query = "SELECT s FROM RpService s WHERE s.serviceName = :serviceName")
//        , @NamedQuery(name = "Service.findByServiceDescription", query = "SELECT s FROM RpService s WHERE s.serviceDescription = :serviceDescription")
//        , @NamedQuery(name = "Service.findByServiceStatus", query = "SELECT s FROM RpService s WHERE s.serviceStatus = :serviceStatus")
//        , @NamedQuery(name = "Service.findByCreateDatetime", query = "SELECT s FROM RpService s WHERE s.createDatetime = :createDatetime")
//        , @NamedQuery(name = "Service.findByCreateUser", query = "SELECT s FROM RpService s WHERE s.createUser = :createUser")
//        , @NamedQuery(name = "Service.findByUpdateDatetime", query = "SELECT s FROM RpService s WHERE s.updateDatetime = :updateDatetime")
//        , @NamedQuery(name = "Service.findByUpdateUser", query = "SELECT s FROM RpService s WHERE s.updateUser = :updateUser")})
public class RpService implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "service_id")
    private Integer serviceId;
    @Column(name = "service_name")
    private String serviceName;
    @Column(name = "service_description")
    private String serviceDescription;
    @Column(name = "service_status")
    private String serviceStatus;
    @Column(name = "create_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDatetime;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "update_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDatetime;
    @Column(name = "update_user")
    private String updateUser;
    @JoinTable(name = "service_resource", joinColumns = {
            @JoinColumn(name = "service_id", referencedColumnName = "service_id")}, inverseJoinColumns = {
            @JoinColumn(name = "resource_id", referencedColumnName = "resource_id")})
    @ManyToMany
    private List<RpResource> rpResourceList;
    @OneToMany(mappedBy = "serviceId")
    private List<RpServicePrice> rpServicePriceList;
    @OneToMany(mappedBy = "serviceId")
    private List<RpOrgServiceRegister> rpOrgServiceRegisterList;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public RpService() {
    }

    public RpService(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @XmlTransient
    public List<RpResource> getRpResourceList() {
        return rpResourceList;
    }

    public void setRpResourceList(List<RpResource> rpResourceList) {
        this.rpResourceList = rpResourceList;
    }

    @XmlTransient
    public List<RpServicePrice> getRpServicePriceList() {
        return rpServicePriceList;
    }

    public void setRpServicePriceList(List<RpServicePrice> rpServicePriceList) {
        this.rpServicePriceList = rpServicePriceList;
    }

    @XmlTransient
    public List<RpOrgServiceRegister> getRpOrgServiceRegisterList() {
        return rpOrgServiceRegisterList;
    }

    public void setRpOrgServiceRegisterList(List<RpOrgServiceRegister> rpOrgServiceRegisterList) {
        this.rpOrgServiceRegisterList = rpOrgServiceRegisterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serviceId != null ? serviceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpService)) {
            return false;
        }
        RpService other = (RpService) object;
        if ((this.serviceId == null && other.serviceId != null) || (this.serviceId != null && !this.serviceId.equals(other.serviceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RpService{" +
                "serviceId=" + serviceId +
                ", serviceName='" + serviceName + '\'' +
                ", serviceDescription='" + serviceDescription + '\'' +
                ", serviceStatus='" + serviceStatus + '\'' +
                ", createDatetime=" + createDatetime +
                ", createUser='" + createUser + '\'' +
                ", updateDatetime=" + updateDatetime +
                ", updateUser='" + updateUser + '\'' +
                ", rpResourceList=" + rpResourceList +
                ", rpServicePriceList=" + rpServicePriceList +
                ", rpOrgServiceRegisterList=" + rpOrgServiceRegisterList +
                '}';
    }

    @PrePersist
    public void prePersist(){
        if(this.createDatetime == null){
            this.createDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

    @PreUpdate
    public void preUpdate(){
        if(this.updateDatetime == null){
            this.updateDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

}
