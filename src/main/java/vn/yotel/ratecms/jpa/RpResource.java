package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "resource_tbl")
@XmlRootElement
//@NamedQueries({
//        @NamedQuery(name = "RpResource.findAll", query = "SELECT r FROM RpResource r")
//        , @NamedQuery(name = "RpResource.findByResourceId", query = "SELECT r FROM RpResource r WHERE r.resourceId = :resourceId")
//        , @NamedQuery(name = "RpResource.findByResourceName", query = "SELECT r FROM RpResource r WHERE r.resourceName = :resourceName")
//        , @NamedQuery(name = "RpResource.findByUrl", query = "SELECT r FROM RpResource r WHERE r.url = :url")
//        , @NamedQuery(name = "RpResource.findByResourceDescription", query = "SELECT r FROM RpResource r WHERE r.resourceDescription = :resourceDescription")
//        , @NamedQuery(name = "RpResource.findByResourceStatus", query = "SELECT r FROM RpResource r WHERE r.resourceStatus = :resourceStatus")
//        , @NamedQuery(name = "RpResource.findByCreateDatetime", query = "SELECT r FROM RpResource r WHERE r.createDatetime = :createDatetime")
//        , @NamedQuery(name = "RpResource.findByCreateUser", query = "SELECT r FROM RpResource r WHERE r.createUser = :createUser")
//        , @NamedQuery(name = "RpResource.findByUpdateDatetime", query = "SELECT r FROM RpResource r WHERE r.updateDatetime = :updateDatetime")
//        , @NamedQuery(name = "RpResource.findByUpdateUser", query = "SELECT r FROM RpResource r WHERE r.updateUser = :updateUser")})
public class RpResource implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "resource_id")
    private Integer resourceId;
    @Column(name = "resource_name")
    private String resourceName;
    @Column(name = "URL")
    private String url;
    @Column(name = "resource_description")
    private String resourceDescription;
    @Column(name = "resource_status")
    private String resourceStatus;
    @Column(name = "create_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDatetime;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "update_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDatetime;
    @Column(name = "update_user")
    private String updateUser;
    @ManyToMany(mappedBy = "rpResourceList")
    private List<RpService> serviceList;

    public RpResource() {
    }

    public RpResource(Integer resourceId) {
        this.resourceId = resourceId;
    }

    public Integer getResourceId() {
        return resourceId;
    }

    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getResourceDescription() {
        return resourceDescription;
    }

    public void setResourceDescription(String resourceDescription) {
        this.resourceDescription = resourceDescription;
    }

    public String getResourceStatus() {
        return resourceStatus;
    }

    public void setResourceStatus(String resourceStatus) {
        this.resourceStatus = resourceStatus;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @XmlTransient
    public List<RpService> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<RpService> serviceList) {
        this.serviceList = serviceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (resourceId != null ? resourceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpResource)) {
            return false;
        }
        RpResource other = (RpResource) object;
        if ((this.resourceId == null && other.resourceId != null) || (this.resourceId != null && !this.resourceId.equals(other.resourceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RpResource{" +
                "resourceId=" + resourceId +
                ", resourceName='" + resourceName + '\'' +
                ", url='" + url + '\'' +
                ", resourceDescription='" + resourceDescription + '\'' +
                ", resourceStatus='" + resourceStatus + '\'' +
                ", createDatetime=" + createDatetime +
                ", createUser='" + createUser + '\'' +
                ", updateDatetime=" + updateDatetime +
                ", updateUser='" + updateUser + '\'' +
                ", serviceList=" + serviceList +
                '}';
    }

    @PrePersist
    public void prePersist(){
        if(this.createDatetime == null){
            this.createDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

    @PreUpdate
    public void preUpdate(){
        if(this.updateDatetime == null){
            this.updateDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }
}
