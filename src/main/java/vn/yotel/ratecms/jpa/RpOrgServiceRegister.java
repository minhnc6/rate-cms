/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "org_service_register")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "RpOrgServiceRegister.findAll", query = "SELECT r FROM RpOrgServiceRegister r")
//    , @NamedQuery(name = "RpOrgServiceRegister.findById", query = "SELECT r FROM RpOrgServiceRegister r WHERE r.id = :id")
//    , @NamedQuery(name = "RpOrgServiceRegister.findByRegisterStatus", query = "SELECT r FROM RpOrgServiceRegister r WHERE r.registerStatus = :registerStatus")
//    , @NamedQuery(name = "RpOrgServiceRegister.findByCreateDatetime", query = "SELECT r FROM RpOrgServiceRegister r WHERE r.createDatetime = :createDatetime")
//    , @NamedQuery(name = "RpOrgServiceRegister.findByCreateUser", query = "SELECT r FROM RpOrgServiceRegister r WHERE r.createUser = :createUser")
//    , @NamedQuery(name = "RpOrgServiceRegister.findByUpdateDatetime", query = "SELECT r FROM RpOrgServiceRegister r WHERE r.updateDatetime = :updateDatetime")
//    , @NamedQuery(name = "RpOrgServiceRegister.findByUpdateUser", query = "SELECT r FROM RpOrgServiceRegister r WHERE r.updateUser = :updateUser")})
public class RpOrgServiceRegister implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "register_status")
    private String registerStatus;
    @Column(name = "create_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDatetime;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "update_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDatetime;
    @Column(name = "update_user")
    private String updateUser;
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")
    @ManyToOne
    private RpOrganization orgId;
    @JoinColumn(name = "service_id", referencedColumnName = "service_id")
    @ManyToOne
    private RpService serviceId;

    public RpOrgServiceRegister() {
    }

    public RpOrgServiceRegister(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegisterStatus() {
        return registerStatus;
    }

    public void setRegisterStatus(String registerStatus) {
        this.registerStatus = registerStatus;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public RpOrganization getOrgId() {
        return orgId;
    }

    public void setOrgId(RpOrganization orgId) {
        this.orgId = orgId;
    }

    public RpService getServiceId() {
        return serviceId;
    }

    public void setServiceId(RpService serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpOrgServiceRegister)) {
            return false;
        }
        RpOrgServiceRegister other = (RpOrgServiceRegister) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RpOrgServiceRegister[ id=" + id + " ]";
    }

    @PrePersist
    public void prePersist(){
        if(this.createDatetime == null){
            this.createDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

    @PreUpdate
    public void preUpdate(){
        if(this.updateDatetime == null){
            this.updateDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

}
