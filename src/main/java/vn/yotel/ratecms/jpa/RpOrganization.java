/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "organization_tbl")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "RpOrganization.findAll", query = "SELECT r FROM RpOrganization r")
//    , @NamedQuery(name = "RpOrganization.findByOrgId", query = "SELECT r FROM RpOrganization r WHERE r.orgId = :orgId")
//    , @NamedQuery(name = "RpOrganization.findByOrgName", query = "SELECT r FROM RpOrganization r WHERE r.orgName = :orgName")
//    , @NamedQuery(name = "RpOrganization.findByOrgCode", query = "SELECT r FROM RpOrganization r WHERE r.orgCode = :orgCode")
//    , @NamedQuery(name = "RpOrganization.findByOrgAddress", query = "SELECT r FROM RpOrganization r WHERE r.orgAddress = :orgAddress")
//    , @NamedQuery(name = "RpOrganization.findByOrgPhone", query = "SELECT r FROM RpOrganization r WHERE r.orgPhone = :orgPhone")
//    , @NamedQuery(name = "RpOrganization.findByOrgTaxNo", query = "SELECT r FROM RpOrganization r WHERE r.orgTaxNo = :orgTaxNo")
//    , @NamedQuery(name = "RpOrganization.findByOrgDescription", query = "SELECT r FROM RpOrganization r WHERE r.orgDescription = :orgDescription")
//    , @NamedQuery(name = "RpOrganization.findByOrgStatus", query = "SELECT r FROM RpOrganization r WHERE r.orgStatus = :orgStatus")
//    , @NamedQuery(name = "RpOrganization.findByCreateDatetime", query = "SELECT r FROM RpOrganization r WHERE r.createDatetime = :createDatetime")
//    , @NamedQuery(name = "RpOrganization.findByUpdateDatetime", query = "SELECT r FROM RpOrganization r WHERE r.updateDatetime = :updateDatetime")
//    , @NamedQuery(name = "RpOrganization.findByUpdateUser", query = "SELECT r FROM RpOrganization r WHERE r.updateUser = :updateUser")
//    , @NamedQuery(name = "RpOrganization.findByCreateUser", query = "SELECT r FROM RpOrganization r WHERE r.createUser = :createUser")
//    , @NamedQuery(name = "RpOrganization.findByIpList", query = "SELECT r FROM RpOrganization r WHERE r.ipList = :ipList")
//    , @NamedQuery(name = "RpOrganization.findByExpireDatetime", query = "SELECT r FROM RpOrganization r WHERE r.expireDatetime = :expireDatetime")})
public class RpOrganization implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "org_id")
    private Integer orgId;
    @Column(name = "org_name")
    private String orgName;
    @Column(name = "org_code")
    private String orgCode;
    @Column(name = "org_address")
    private String orgAddress;
    @Column(name = "org_phone")
    private String orgPhone;
    @Column(name = "org_tax_no")
    private String orgTaxNo;
    @Column(name = "org_description")
    private String orgDescription;
    @Column(name = "org_status")
    private String orgStatus;
    @Column(name = "create_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDatetime;
    @Column(name = "update_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDatetime;
    @Column(name = "update_user")
    private String updateUser;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "ip_list")
    private String ipList;
    @Column(name = "expire_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expireDatetime;
    @Lob
    @Column(name = "token")
    private String token;
    @OneToMany(mappedBy = "orgId")
    private List<RpProduct> rpProductCollection;
    @OneToMany(mappedBy = "orgId")
    private List<RpServicePrice> rpServicePriceCollection;
    @OneToMany(mappedBy = "orgId")
    private List<RpOrgServiceRegister> rpOrgServiceRegisterCollection;
    @Column(name = "org_type")
    private byte orgType;
    public RpOrganization() {
    }

    public byte getOrgType() {
        return orgType;
    }

    public void setOrgType(byte orgType) {
        this.orgType = orgType;
    }

    public RpOrganization(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgAddress() {
        return orgAddress;
    }

    public void setOrgAddress(String orgAddress) {
        this.orgAddress = orgAddress;
    }

    public String getOrgPhone() {
        return orgPhone;
    }

    public void setOrgPhone(String orgPhone) {
        this.orgPhone = orgPhone;
    }

    public String getOrgTaxNo() {
        return orgTaxNo;
    }

    public void setOrgTaxNo(String orgTaxNo) {
        this.orgTaxNo = orgTaxNo;
    }

    public String getOrgDescription() {
        return orgDescription;
    }

    public void setOrgDescription(String orgDescription) {
        this.orgDescription = orgDescription;
    }

    public String getOrgStatus() {
        return orgStatus;
    }

    public void setOrgStatus(String orgStatus) {
        this.orgStatus = orgStatus;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getIpList() {
        return ipList;
    }

    public void setIpList(String ipList) {
        this.ipList = ipList;
    }

    public Date getExpireDatetime() {
        return expireDatetime;
    }

    public void setExpireDatetime(Date expireDatetime) {
        this.expireDatetime = expireDatetime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlTransient
    public List<RpProduct> getRpProductCollection() {
        return rpProductCollection;
    }

    public void setRpProductCollection(List<RpProduct> rpProductCollection) {
        this.rpProductCollection = rpProductCollection;
    }

    @XmlTransient
    public List<RpServicePrice> getRpServicePriceCollection() {
        return rpServicePriceCollection;
    }

    public void setRpServicePriceCollection(List<RpServicePrice> rpServicePriceCollection) {
        this.rpServicePriceCollection = rpServicePriceCollection;
    }

    @XmlTransient
    public List<RpOrgServiceRegister> getRpOrgServiceRegisterCollection() {
        return rpOrgServiceRegisterCollection;
    }

    public void setRpOrgServiceRegisterCollection(List<RpOrgServiceRegister> rpOrgServiceRegisterCollection) {
        this.rpOrgServiceRegisterCollection = rpOrgServiceRegisterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orgId != null ? orgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpOrganization)) {
            return false;
        }
        RpOrganization other = (RpOrganization) object;
        if ((this.orgId == null && other.orgId != null) || (this.orgId != null && !this.orgId.equals(other.orgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.RpOrganization[ orgId=" + orgId + " ]";
    }

}
