/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "action_audit")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "RpActionAudit.findAll", query = "SELECT r FROM RpActionAudit r")
//    , @NamedQuery(name = "RpActionAudit.findByAuditId", query = "SELECT r FROM RpActionAudit r WHERE r.rpActionAuditPK.auditId = :auditId")
//    , @NamedQuery(name = "RpActionAudit.findByActionDatetime", query = "SELECT r FROM RpActionAudit r WHERE r.rpActionAuditPK.actionDatetime = :actionDatetime")
//    , @NamedQuery(name = "RpActionAudit.findByUserName", query = "SELECT r FROM RpActionAudit r WHERE r.userName = :userName")
//    , @NamedQuery(name = "RpActionAudit.findByUserAction", query = "SELECT r FROM RpActionAudit r WHERE r.userAction = :userAction")
//    , @NamedQuery(name = "RpActionAudit.findByObject", query = "SELECT r FROM RpActionAudit r WHERE r.object = :object")
//    , @NamedQuery(name = "RpActionAudit.findByDetail", query = "SELECT r FROM RpActionAudit r WHERE r.detail = :detail")})
public class RpActionAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RpActionAuditPK rpActionAuditPK;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "user_action")
    private String userAction;
    @Column(name = "object")
    private String _object;
    @Column(name = "detail")
    private String _detail;

    public RpActionAudit() {
    }

    public RpActionAudit(RpActionAuditPK rpActionAuditPK) {
        this.rpActionAuditPK = rpActionAuditPK;
    }

    public RpActionAudit(int auditId, Date actionDatetime) {
        this.rpActionAuditPK = new RpActionAuditPK(auditId, actionDatetime);
    }

    public RpActionAuditPK getRpActionAuditPK() {
        return rpActionAuditPK;
    }

    public void setRpActionAuditPK(RpActionAuditPK rpActionAuditPK) {
        this.rpActionAuditPK = rpActionAuditPK;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAction() {
        return userAction;
    }

    public void setUserAction(String userAction) {
        this.userAction = userAction;
    }

    public String getObject() {
        return _object;
    }

    public void setObject(String object) {
        this._object = object;
    }

    public String getDetail() {
        return _detail;
    }

    public void setDetail(String detail) {
        this._detail = detail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rpActionAuditPK != null ? rpActionAuditPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpActionAudit)) {
            return false;
        }
        RpActionAudit other = (RpActionAudit) object;
        if ((this.rpActionAuditPK == null && other.rpActionAuditPK != null) || (this.rpActionAuditPK != null && !this.rpActionAuditPK.equals(other.rpActionAuditPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.RpActionAudit[ rpActionAuditPK=" + rpActionAuditPK + " ]";
    }

}
