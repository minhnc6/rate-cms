package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "product")
@XmlRootElement
//@NamedQueries({
//        @NamedQuery(name = "RpProduct.findAll", query = "SELECT p FROM RpProduct p")
//        , @NamedQuery(name = "RpProduct.findByProductId", query = "SELECT p FROM RpProduct p WHERE p.productId = :productId")
//        , @NamedQuery(name = "RpProduct.findByProductName", query = "SELECT p FROM RpProduct p WHERE p.productName = :productName")
//        , @NamedQuery(name = "RpProduct.findByProductDescription", query = "SELECT p FROM RpProduct p WHERE p.productDescription = :productDescription")
//        , @NamedQuery(name = "RpProduct.findByProductStatus", query = "SELECT p FROM RpProduct p WHERE p.productStatus = :productStatus")
//        , @NamedQuery(name = "RpProduct.findByCreateDatetime", query = "SELECT p FROM RpProduct p WHERE p.createDatetime = :createDatetime")
//        , @NamedQuery(name = "RpProduct.findByCreateUser", query = "SELECT p FROM RpProduct p WHERE p.createUser = :createUser")
//        , @NamedQuery(name = "RpProduct.findByUpdateDatetime", query = "SELECT p FROM RpProduct p WHERE p.updateDatetime = :updateDatetime")
//        , @NamedQuery(name = "RpProduct.findByUpdateUser", query = "SELECT p FROM RpProduct p WHERE p.updateUser = :updateUser")})
public class RpProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "product_name")
    private String productName;
    @Column(name = "product_description")
    private String productDescription;
    @Column(name = "product_status")
    private String productStatus;
    @Column(name = "create_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDatetime;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "update_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDatetime;
    @Column(name = "update_user")
    private String updateUser;
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")
    @ManyToOne
    private RpOrganization orgId;

    public RpProduct() {
    }

    public RpProduct(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public RpOrganization getOrgId() {
        return orgId;
    }

    public void setOrgId(RpOrganization orgId) {
        this.orgId = orgId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RpProduct)) {
            return false;
        }
        RpProduct other = (RpProduct) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RpProduct{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productDescription='" + productDescription + '\'' +
                ", productStatus='" + productStatus + '\'' +
                ", createDatetime=" + createDatetime +
                ", createUser='" + createUser + '\'' +
                ", updateDatetime=" + updateDatetime +
                ", updateUser='" + updateUser + '\'' +
                ", orgId=" + orgId +
                '}';
    }

    @PrePersist
    public void prePersist(){
        if(this.createDatetime == null){
            this.createDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

    @PreUpdate
    public void preUpdate(){
        if(this.updateDatetime == null){
            this.updateDatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }
}
