package vn.yotel.ratecms.jpa;

public class RpDetailInformation/* implements Serializable*/ {
    private String isdn;
    private String service;
    private String requestDate;
    private String orgCode;
    private String requestStatus;
    private String responseStatus;
    private String responseDate;
    private String transId;
    private String status;
    private String reqRequest;
    private String reqResponse;
    private String resRequest;
    private String resResponse;

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReqRequest() {
        return reqRequest;
    }

    public void setReqRequest(String reqRequest) {
        this.reqRequest = reqRequest;
    }

    public String getReqResponse() {
        return reqResponse;
    }

    public void setReqResponse(String reqResponse) {
        this.reqResponse = reqResponse;
    }

    public String getResRequest() {
        return resRequest;
    }

    public void setResRequest(String resRequest) {
        this.resRequest = resRequest;
    }

    public String getResResponse() {
        return resResponse;
    }

    public void setResResponse(String resResponse) {
        this.resResponse = resResponse;
    }

    @Override
    public String toString() {
        return "RpDetailInformation{" +
                "isdn='" + isdn + '\'' +
                ", service='" + service + '\'' +
                ", requestDate='" + requestDate + '\'' +
                ", orgCode='" + orgCode + '\'' +
                ", requestStatus='" + requestStatus + '\'' +
                ", responseStatus='" + responseStatus + '\'' +
                ", responseDate='" + responseDate + '\'' +
                ", transId='" + transId + '\'' +
                ", status='" + status + '\'' +
                ", reqRequest='" + reqRequest + '\'' +
                ", reqResponse='" + reqResponse + '\'' +
                ", resRequest='" + resRequest + '\'' +
                ", resResponse='" + resResponse + '\'' +
                '}';
    }
}
