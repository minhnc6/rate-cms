/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.yotel.ratecms.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Embeddable
public class CampaignDailyResultPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "result_id")
    private int resultId;
    @Basic(optional = false)
    @Column(name = "receive_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date receiveDatetime;

    public CampaignDailyResultPK() {
    }

    public CampaignDailyResultPK(int resultId, Date receiveDatetime) {
        this.resultId = resultId;
        this.receiveDatetime = receiveDatetime;
    }

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public Date getReceiveDatetime() {
        return receiveDatetime;
    }

    public void setReceiveDatetime(Date receiveDatetime) {
        this.receiveDatetime = receiveDatetime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) resultId;
        hash += (receiveDatetime != null ? receiveDatetime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampaignDailyResultPK)) {
            return false;
        }
        CampaignDailyResultPK other = (CampaignDailyResultPK) object;
        if (this.resultId != other.resultId) {
            return false;
        }
        if ((this.receiveDatetime == null && other.receiveDatetime != null) || (this.receiveDatetime != null && !this.receiveDatetime.equals(other.receiveDatetime))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.CampaignDailyResultPK[ resultId=" + resultId + ", receiveDatetime=" + receiveDatetime + " ]";
    }

}
