package vn.yotel.ratecms.config.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import vn.yotel.admin.jpa.AuthUser;

public class AppAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(AppAuthenticationSuccessHandler.class);
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        AuthUser authUser = (AuthUser) authentication.getPrincipal();
        String targetLink = "/";
        request.getSession().setAttribute("username", authUser.getUserName());
        List<GrantedAuthority> lstAuthorities = new ArrayList<GrantedAuthority>(authentication.getAuthorities());
        for (GrantedAuthority grantedAuthority : lstAuthorities) {
            if (grantedAuthority.getAuthority().equals("Administrators")) {
                targetLink = "/organization/list.html";
                break;
            } else if (grantedAuthority.getAuthority().equals("Users")) {
                targetLink = "/report/doisoat_chitiet.html";
                break;
            } else if (grantedAuthority.getAuthority().equals("Accounting")) {
                targetLink = "/report/doisoat_chitiet.html";
                break;
            } else {
                targetLink = "/report/doisoat_chitiet.html";
                break;
            }
        }

        if (response.isCommitted()) {
            logger.warn("Response has already been committed. Unable to redirect to " + targetLink);
        } else {
            request.getSession().setAttribute("dashboardLink", targetLink);
            request.getSession().setAttribute("userType", (int) authUser.getUserType());
            redirectStrategy.sendRedirect(request, response, targetLink);
        }
    }

}
