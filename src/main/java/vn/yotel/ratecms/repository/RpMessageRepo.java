package vn.yotel.ratecms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.RpMessage;

import java.util.List;

public interface RpMessageRepo extends JpaRepository<RpMessage, Integer> {
    RpMessage findByMsgName(String name);
    List<RpMessage> findByMsgStatus(String status);

    @Query(value="SELECT e FROM RpMessage e WHERE "
            + " ( e.msgStatus = :status OR :status IS NULL) "
            + " AND ( e.msgName LIKE :name OR :name IS NULL)"
            ,nativeQuery=false)
    List<RpMessage> findList(@Param("name") String name,
                                         @Param("status") String status);
}
