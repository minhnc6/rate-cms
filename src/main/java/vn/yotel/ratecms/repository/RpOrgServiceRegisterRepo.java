package vn.yotel.ratecms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.RpOrgServiceRegister;
import vn.yotel.ratecms.jpa.RpOrganization;
import vn.yotel.ratecms.jpa.RpService;

import java.util.List;

public interface RpOrgServiceRegisterRepo extends JpaRepository<RpOrgServiceRegister, Integer> {
    List<RpOrgServiceRegister> findByRegisterStatus(String status);

    List<RpOrgServiceRegister> findByOrgIdAndRegisterStatus(RpOrganization orgId, String status);

    @Query(value = "SELECT e FROM RpOrgServiceRegister e "
            + " WHERE (registerStatus = :status OR :status IS NULL) "
            + " AND (orgId = :orgId OR :orgId IS NULL)"
            + " AND (serviceId = :serviceId OR :serviceId IS NULL)", nativeQuery = false)
    Page<RpOrgServiceRegister> findList(@Param(value = "orgId") RpOrganization orgId,
                                           @Param(value = "serviceId") RpService serviceId,
                                           @Param(value = "status") String status, Pageable page);
}
