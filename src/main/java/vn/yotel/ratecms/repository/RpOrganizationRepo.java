package vn.yotel.ratecms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.RpOrganization;

import java.util.List;

public interface RpOrganizationRepo extends JpaRepository<RpOrganization, Integer> {
    RpOrganization findByOrgName(String name);
    RpOrganization findByOrgCode(String code);
    List<RpOrganization> findByOrgStatus(String status);

    @Query(value="SELECT e FROM RpOrganization e WHERE "
            + " ( e.orgStatus = :status OR :status IS NULL) "
            + " AND ( e.orgName LIKE :name OR :name IS NULL)"
            ,nativeQuery=false)
    List<RpOrganization> findList(@Param("name") String name,
                                         @Param("status") String status);

    @Query(value = "SELECT e FROM RpOrganization e "
            + " WHERE (orgName like :name OR :name IS NULL)"
            + " AND (orgCode like :code OR :code IS NULL)"
            + " AND (orgStatus = :status OR :status IS NULL) ", nativeQuery = false)
    Page<RpOrganization> findPage(@Param(value = "name") String name, @Param(value = "code") String code,
                                         @Param(value = "status") String status, Pageable page);
}
