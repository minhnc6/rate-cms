package vn.yotel.ratecms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.CampaignDailyResult;

import java.util.Date;
import java.util.List;

public interface RpCampaignDailyResultRepo extends JpaRepository<CampaignDailyResult, Integer> {
    @Query(value = "SELECT e FROM CampaignDailyResult e "
            + " WHERE (e.campaignDailyResultPK.receiveDatetime BETWEEN :fromDate AND :toDate)"
            + " AND (isdn = :msisdn OR :msisdn IS NULL)"
            + " AND (orgId like :orgId OR :orgId IS NULL)"
            + " AND (productId like :productId OR :productId IS NULL)"
            + " AND (msgId like :msgId OR :msgId IS NULL)"
            + " AND (campaignId like :campaignId OR :campaignId IS NULL)", nativeQuery = false)
    Page<CampaignDailyResult> findList(@Param(value = "fromDate") Date fromDate, @Param(value = "toDate") Date toDate,
                                         @Param(value = "msisdn") String msisdn, @Param(value = "orgId") Integer orgId,
                                         @Param(value = "productId") Integer productId, @Param(value = "msgId") Integer msgId,
                                         @Param(value = "campaignId") Integer campaignId,Pageable page);

    @Query(value = "SELECT e FROM CampaignDailyResult e "
            + " WHERE (e.campaignDailyResultPK.receiveDatetime BETWEEN :fromDate AND :toDate)"
            + " AND (isdn = :msisdn OR :msisdn IS NULL)"
            + " AND (orgId like :orgId OR :orgId IS NULL)"
            + " AND (productId like :productId OR :productId IS NULL)"
            + " AND (msgId like :msgId OR :msgId IS NULL)"
            + " AND (campaignId like :campaignId OR :campaignId IS NULL)", nativeQuery = false)
    List<CampaignDailyResult> findList(@Param(value = "fromDate") Date fromDate, @Param(value = "toDate") Date toDate,
                                       @Param(value = "msisdn") String msisdn, @Param(value = "orgId") Integer orgId,
                                       @Param(value = "productId") Integer productId, @Param(value = "msgId") Integer msgId,
                                       @Param(value = "campaignId") Integer campaignId);

    @Query(value="SELECT date(receive_datetime), org_id, count(isdn) "
            + " FROM campaign_daily_result "
            + " where receive_datetime between :fromDate AND :toDate "
            + " and (org_id = :orgId OR :orgId IS NULL) "
            + " group by date(receive_datetime), org_id "
            + " order by date(receive_datetime) desc, org_id desc "
            ,nativeQuery=true)
    List<Object[]> findReportDailyResult(@Param(value = "fromDate") Date fromDate,
                                         @Param(value = "toDate") Date toDate,
                                         @Param(value = "orgId") Integer orgId);
}
