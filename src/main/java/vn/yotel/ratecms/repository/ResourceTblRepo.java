package vn.yotel.ratecms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.RpResource;

import java.util.List;

public interface ResourceTblRepo extends JpaRepository<RpResource, Integer> {
    RpResource findByResourceName(String name);
    List<RpResource> findByResourceStatus(String status);

    @Query(value="SELECT e FROM RpResource e WHERE "
            + " ( e.resourceStatus = :status OR :status IS NULL) "
            + " AND ( e.resourceName LIKE :name OR :name IS NULL)"
            ,nativeQuery=false)
    List<RpResource> findList(@Param("name") String name,
                              @Param("status") String status);
}
