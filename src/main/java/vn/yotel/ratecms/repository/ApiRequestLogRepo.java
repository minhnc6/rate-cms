package vn.yotel.ratecms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.ApiRequestLog;

import java.util.Date;
import java.util.List;

public interface ApiRequestLogRepo extends JpaRepository<ApiRequestLog, Integer> {
    @Query(value = "SELECT e FROM ApiRequestLog e "
            + " WHERE (e.apiRequestLogPK.requestDatetime BETWEEN :fromDate AND :toDate)"
            + " AND e.serviceCode IN ('FI_Scoring', 'Telco_Scoring', 'FI_VerifyInfo', 'Telco_VerifyInfo') "
            + " AND (e.organizationCode = :orgCode OR :orgCode IS NULL) "
            + " AND (e.organizationCode IS NOT NULL) "
            + " AND (e.charge IS NOT NULL) ", nativeQuery = false)
    List<ApiRequestLog> findByRequestDatetime(@Param(value = "fromDate") Date fromDate,
                                              @Param(value = "toDate") Date toDate,
                                              @Param(value = "orgCode") String orgCode);

    @Query(value = "SELECT log.organization_code,\n" +
            "sum(CASE WHEN log.service_code IN ( 'FI_Scoring', 'Telco_Scoring') and log.charge = 0 then 1 else 0 end) as freePoi,\n" +
            "sum(case when log.service_code in ( 'FI_Scoring', 'Telco_Scoring') and log.charge = 1 then 1 else 0 end) as paidPoi,\n" +
            "sum(case when log.service_code in ( 'FI_Scoring', 'Telco_Scoring') then 1 else 0 end) as allPoi,\n" +
            "sum(case when log.service_code in ( 'FI_VerifyInfo', 'Telco_VerifyInfo') and log.charge = 0 then 1 else 0 end) as freeCus,\n" +
            "sum(case when log.service_code in ( 'FI_VerifyInfo', 'Telco_VerifyInfo') and log.charge = 1 then 1 else 0 end) as paidCus,\n" +
            "sum(case when  log.service_code in ( 'FI_VerifyInfo', 'Telco_VerifyInfo') then 1 else 0 end) as allCus,\n" +
            "sum(case when log.service_code in ( 'Frauding') and log.charge = 0 then 1 else 0 end) as freeFrau,\n" +
            "sum(case when log.service_code in ( 'Frauding') and log.charge = 1 then 1 else 0 end) as paidFrau,\n" +
            "sum(case when  log.service_code in ('Frauding') then 1 else 0 end) as allFrau\n" +
            "FROM (\n" +
            "select s.isdn,s.service_code,s.request_datetime as req_datetime, r.organization_code,\n" +
            " r.request_datetime as res_datetime,s.trans_id , r.charge\n" +
            " from\n" +
            " (\n" +
            "select a.service_code, a.isdn,a.trans_id, a.organization_code, a.request_datetime\n" +
            "from api_request_log a, organization_tbl b\n" +
            "where a.organization_code = b.org_code \n" +
            "and (case b.org_type when 1 then a.direction = '1'\n" +
            "else a.direction = '2' end )\n" +
            "and a.service_code in ('FI_Scoring', 'Telco_Scoring', 'FI_VerifyInfo', 'Telco_VerifyInfo', 'Frauding')\n" +
            "and (organization_code = :orgCode OR :orgCode IS NULL)\n" +
            "and a.request_datetime between :fromDate AND :toDate\n" +
            "and a.status = 0) s,\n" +
            "(\n" +
            "select a.trans_id, a.request_datetime, a.organization_code, a.charge\n" +
            "from api_request_log a, organization_tbl b\n" +
            "where a.organization_code = b.org_code \n" +
            "and (case b.org_type when 1 then a.direction = '2'\n" +
            "else a.direction = '1' end )\n" +
            "and a.service_code in ('FI_Scoring', 'Telco_Scoring', 'FI_VerifyInfo', 'Telco_VerifyInfo', 'Frauding')\n" +
            "and (organization_code = :orgCode OR :orgCode IS NULL)\n" +
            "and a.request_datetime between :fromDate AND date_add(:toDate, interval 7 day)\n" +
            "and a.status = 0) r\n" +
            "where s.trans_id = r.trans_id\n" +
            "and s.organization_code = r.organization_code) as log\n" +
            "group by log.organization_code", nativeQuery = true)
    List<Object[]> findReport(@Param(value = "fromDate") Date fromDate,
                              @Param(value = "toDate") Date toDate,
                              @Param(value = "orgCode") String orgCode);

    @Query(value = "select s.isdn,\n" +
            "case when s.service_code in ('FI_Scoring', 'Telco_Scoring') then 'Truy vấn điểm'\n" +
            "when s.service_code in ('Frauding') then 'Chấm điểm gian lận'\n" +
            "else 'Xác minh' end as service,\n" +
            "date_format(s.request_datetime,'%d/%m/%Y %H:%i:%s') as req_datetime, s.organization_code,\n" +
            "case when s.request_status = 1 then 'Thành công' else 'Thất bại' end as request_status,\n" +
            "case when r.response_status = 1 then 'Thành công' else 'Thất bại' end as response_status,\n" +
            "date_format(r.request_datetime,'%d/%m/%Y %H:%i:%s') as res_datetime,s.trans_id,\n" +
            "case when s.request_status = 1 and r.response_status = 1 then 'Thành công'\n" +
            "else 'Thất bại' end as status,\n" +
            "s.request as req_request,s.response as req_response,\n" +
            "r.request as res_request,r.response as res_response\n" +
            "from\n" +
            "(\n" +
            "select a.service_code, a.isdn,a.trans_id, a.organization_code, a.request_datetime, \n" +
            "case when a.status = 0 then 1 \n" +
            "else 0 end as request_status,\n" +
            "a.request, a.response\n" +
            "from api_request_log a, organization_tbl b\n" +
            "where a.organization_code = b.org_code \n" +
            "and (case b.org_type when 1 then a.direction = '1'\n" +
            "else a.direction = '2' end )\n" +
            "and a.service_code in ('FI_Scoring', 'Telco_Scoring', 'FI_VerifyInfo', 'Telco_VerifyInfo', 'Frauding')\n" +
            "and (organization_code = :orgCode OR :orgCode IS NULL)\n" +
            "and a.request_datetime between :fromDate AND :toDate\n" +
            "and a.status is not null) s \n" +
            "left join\n" +
            "(\n" +
            "select a.trans_id, a.request_datetime, a.organization_code,\n" +
            "case when a.status = 0 then 1 \n" +
            "else 0 end as response_status,\n" +
            "a.request, a.response\n" +
            "from api_request_log a, organization_tbl b\n" +
            "where a.organization_code = b.org_code \n" +
            "and (case b.org_type when 1 then a.direction = '2'\n" +
            "else a.direction = '1' end )\n" +
            "and a.service_code in ('FI_Scoring', 'Telco_Scoring', 'FI_VerifyInfo', 'Telco_VerifyInfo', 'Frauding')\n" +
            "and (organization_code = :orgCode OR :orgCode IS NULL)\n" +
            "and a.request_datetime between :fromDate AND date_add(:toDate, interval 7 day)\n" +
            ") r on s.trans_id = r.trans_id and s.organization_code = r.organization_code"
            , nativeQuery = true)
    List<Object[]> findByRequestDatetimeNew(@Param(value = "fromDate") Date fromDate,
                                            @Param(value = "toDate") Date toDate,
                                            @Param(value = "orgCode") String orgCode);
}


