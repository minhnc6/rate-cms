package vn.yotel.ratecms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.RpActionAudit;

import java.util.Date;
import java.util.List;

public interface RpActionAuditRepo extends JpaRepository<RpActionAudit, Integer> {
    @Query(value = "SELECT e FROM RpActionAudit e "
            + " WHERE (e.rpActionAuditPK.actionDatetime BETWEEN :fromDate AND :toDate)"
            + " AND (userName like :userName OR :userName IS NULL)"
            + " AND (userAction like :userAction OR :userAction IS NULL)"
            + " AND (_object like :_object OR :_object IS NULL) "
            + " ORDER BY e.rpActionAuditPK.actionDatetime DESC ", nativeQuery = false)
    Page<RpActionAudit> findList(@Param(value = "fromDate") Date fromDate, @Param(value = "toDate") Date toDate,
                                 @Param(value = "userName") String userName, @Param(value = "userAction") String userAction,
                                 @Param(value = "_object") String _object, Pageable page);
}
