package vn.yotel.ratecms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.Campaign;

import java.util.Date;
import java.util.List;

public interface CampaignRepo extends JpaRepository<Campaign, Integer> {

}
