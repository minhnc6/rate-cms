package vn.yotel.ratecms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.RpProduct;

import java.util.List;

public interface RpProductRepo extends JpaRepository<RpProduct, Integer> {
    RpProduct findByProductName(String name);
    List<RpProduct> findByProductStatus(String status);
    List<RpProduct> findByOrgIdAndProductStatus(Integer orgId, String status);

    @Query(value="SELECT e FROM RpProduct e WHERE "
            + " ( e.productStatus = :status OR :status IS NULL) "
            + " AND ( e.productName LIKE :name OR :name IS NULL)"
            ,nativeQuery=false)
    List<RpProduct> findByNameAndStatus(@Param("name") String name,
                                         @Param("status") String status);

    @Query(value="SELECT e FROM RpProduct e WHERE "
            + " ( e.productStatus = :status OR :status IS NULL) "
            + " AND ( e.productName LIKE :name OR :name IS NULL)"
            ,nativeQuery=false)
    Page<RpProduct> findList(@Param("name") String name,
                             @Param("status") String status, Pageable page);
}
