package vn.yotel.ratecms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.yotel.ratecms.jpa.RpService;
import java.util.List;

public interface RpServiceRepo extends JpaRepository<RpService, Integer> {
    RpService findByServiceName(String name);
    List<RpService> findByServiceStatus(String status);

    @Query(value="SELECT e FROM RpService e WHERE "
            + " ( e.serviceStatus = :status OR :status IS NULL) "
            + " AND ( e.serviceName LIKE :name OR :name IS NULL)"
            ,nativeQuery=false)
    List<RpService> findList(@Param("name") String name,
                             @Param("status") String status);
}
