package vn.yotel.ratecms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.yotel.ratecms.jpa.UserOrg;

public interface UserOrgRepo extends JpaRepository<UserOrg, Integer> {
    UserOrg findByUserId(Long userId);
}
