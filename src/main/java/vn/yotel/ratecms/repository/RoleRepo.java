package vn.yotel.ratecms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.yotel.admin.jpa.Role;

import java.util.List;

@Repository(value = "roleRepo")
public interface RoleRepo extends JpaRepository<Role,Long> {

    @Query(value = "SELECT r FROM Role r WHERE r.status=:status")
    public List<Role> findByStatus(@Param(value = "status") Integer status);
}
