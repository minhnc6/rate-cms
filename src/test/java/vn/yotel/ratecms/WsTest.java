package vn.yotel.ratecms;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import vn.yotel.commons.model.LinkModel;
import vn.yotel.commons.util.SaaJSoapClient;

public class WsTest {

	@Test
	public void testWsMoRequest() throws Exception {
        String _funcWsUrl = "http://91.241.95.137:9003";
        List<LinkModel> childElements = new ArrayList<LinkModel>();
		childElements.add(new LinkModel("username", "viettel"));
		childElements.add(new LinkModel("password", "vi3t3l"));
		childElements.add(new LinkModel("source", "967744288"));
		childElements.add(new LinkModel("dest", "9139"));
		childElements.add(new LinkModel("content", "A"));

		SaaJSoapClient saaJSoapClient = new SaaJSoapClient(_funcWsUrl + "/vvp/molistener?wsdl", "xsd", "",
				"http://mtws/xsd", "moRequest", childElements);
		saaJSoapClient.createSoapRequest();
		String soapMessageRequest = saaJSoapClient.getSoapMessageRequest();
		HttpResponse<String> httpResp = Unirest.post(_funcWsUrl + "/vvp/molistener?wsdl")
				.header("Content-Type", "text/xml;charset=UTF-8")
				.header("SOAPAction", "")
				.body(soapMessageRequest)
				.asString();
		String soapMessageResponse = httpResp.getBody();
		System.out.println(soapMessageResponse);
		saaJSoapClient.setSoapMessageResponse(soapMessageResponse);
		String returnCode = saaJSoapClient.getValueByTagNameFromResponse("return");
		System.out.println(returnCode);
	}

	@Test
	public void testRestAPI() throws UnirestException {
		HttpResponse<String> httpResp = Unirest.post("http://localhost:8081/footballgame/v1" + "/mt")
				.field("subid", "")
				.field("mt", "")
				.field("shortcode", "")
				.field("motransid", "")
				.field("type", "")
				.field("username", "")
				.field("password", "")
				.field("transid", "")
				.asString();
		String msg = httpResp.getBody();
		System.out.println(msg);
	}

}
