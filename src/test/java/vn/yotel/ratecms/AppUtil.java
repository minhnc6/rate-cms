package vn.yotel.ratecms;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.security.crypto.codec.Hex;

public class AppUtil {

//	@Test
//	public void test() {
//		Holder<String> transactionID = new Holder<String>("12");
//		Holder<Integer> errorCode = new Holder<Integer>(0);
//		long lduration = 100l;
//		String info = String.format("transactionID: %s, errorCode: %d, time: %d", transactionID.value, errorCode.value, lduration);
//		System.out.println(info);
//	}
//
	@Test
	public void testEncryptPassword() {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword("jasypt");
		String username = encryptor.encrypt("rateplus");
		String password = encryptor.encrypt("RatepluS@123");
		System.out.println("----------------------------------- Username: " + username );
		System.out.println("----------------------------------- pass: " + password );
	}

	@Test
	public void testDecryptPassword() {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword("jasypt");
		String username = encryptor.decrypt("auimAaaDKsLoScAYqQ6jSQ==");
		String password = encryptor.decrypt("esQYQ3grltMY1fy9w2DF1Oja8N+3Gud/");
		System.out.println("----------------------------------- Username: " + username );
		System.out.println("----------------------------------- pass: " + password );
	}
//
//	@Test
//	public void testDevice() {
//		System.out.println(1 % 2);
//		NumberFormat formatter = new DecimalFormat("#,##0");
//		System.err.println(formatter.format(1000000l));
//	}
//
//
//	@Test
//	public void testRestAPI() throws UnirestException {
//		HttpResponse<String> httpResp = Unirest.post("http://localhost:8080/footballgame/v1" + "/mt")
//				.field("subid", "")
//				.field("mt", "")
//				.field("shortcode", "")
//				.field("motransid", "")
//				.field("type", "")
//				.field("username", "")
//				.field("password", "")
//				.field("transid", "")
//				.asString();
//		String msg = httpResp.getBody();
//		System.out.println(msg);
//	}

    @Test
    public void testMo() {
        DateTime toDay = new DateTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date fromDate = toDay.withTime(23, 59, 59, 999).toDate();

        Integer currentDay = toDay.getDayOfMonth() - 2;
        Integer currentMonth = toDay.getMonthOfYear();
        Integer currentYear = toDay.getYear();
        Date toDate = new DateTime(currentYear, currentMonth, currentDay, 23, 59, 59).toDate();

        System.out.println(dateFormat.format(fromDate));
        System.out.println(dateFormat.format(toDate));
        System.out.println(currentDay);
        System.out.println(currentMonth);
        System.out.println(currentYear);
    }

    @Test
    public void testDecode() throws UnsupportedEncodingException {
    	String hexString = "FEFF0051007500790020006B00680061006300680020006400610020007400680061006D002000670069006100200044005600200059004F004C004F00200076006F006900200063006F00200068006F006900200063006800690065006E0020007400680061006E0067002000310032003000200074007200690065007500200064006F006E00670020007400690065006E0020006D00610074002000670069006100690020006300680075006E0067002000630075006F0063002E00200051007500790020006B0068006100630068002000640061002000740069006300680020006C00750079002000640075006F006300200032003000300020006400690065006D002E00200053006F0061006E0020005400480045004D0020006700750069002000390031003300390020006400650020006E00680061006E0020006C00610069002000630061007500200068006F00690020006300680075006100200074007200610020006C006F0069002000670061006E0020006E00680061007400200028006E0065007500200051007500790020006B006800610063006800200064006100200074007200610020006C006F00690020006800650074002000630061007500200068006F0069002C00200076007500690020006C006F006E006700200062006F0020007100750061002000630075002000700068006100700020005400480045004D0029002E";
    	byte[] bytes = Hex.decode(hexString);
    	System.out.println(new String(bytes, "UTF-16"));
    }

}
